import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
const headers = { 'Content-Type': 'multipart/form-data'}

@Injectable({  providedIn: 'root' })


export class EmailTemplateService {

  constructor(private http: HttpClient) { }

  add(details) { 
		return this.http.post(environment.apiUrl+'/email-template/add',details);   		
  }

  edit(details){
    return this.http.post(environment.apiUrl+'/email-template/edit',details);
  }

  delete(id){
    return this.http.get(environment.apiUrl+'/email-template/delete/'+id);
  }

  updateStatus(id,status){
    return this.http.get(environment.apiUrl+'/email-template/update-status/'+id+"/"+status);
  }

  view(id) { 
    return this.http.get(environment.apiUrl+'/email-template/view/'+id);   		
  }

  getTemplateDetails() { 
    return this.http.get(environment.apiUrl+'/email-template/get-details');   		
  } 
 
}
