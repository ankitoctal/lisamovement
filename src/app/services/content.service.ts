import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  constructor(private http: HttpClient) { }

  add(details) { 
		return this.http.post(environment.apiUrl+'/content-manager/add',details);   		
  } 
  edit(details){
    return this.http.post(environment.apiUrl+'/content-manager/edit',details);
  }
  delete(id){
    return this.http.get(environment.apiUrl+'/content-manager/delete/'+id);
  }
  getDetails() { 
    return this.http.get(environment.apiUrl+'/content-manager/get-details');   		
  } 
  view(id) { 
    return this.http.get(environment.apiUrl+'/content-manager/view/'+id);   		
  } 
}
