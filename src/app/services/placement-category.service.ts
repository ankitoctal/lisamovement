import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class PlacementCategoryService {

  constructor(private http: HttpClient) { }

  add(details:any) { 
		return this.http.post(environment.apiUrl+'/placement-category/add',details);   		
  } 
  edit(details:any){
    return this.http.post(environment.apiUrl+'/placement-category/edit',details);
  }
  delete(id:string){
    return this.http.get(environment.apiUrl+'/placement-category/delete/'+id);
  }
  getDetails() { 
    return this.http.get(environment.apiUrl+'/placement-category/get-details');   		
  } 
  view(id: string) { 
    return this.http.get(environment.apiUrl+'/placement-category/view/'+id);   		
  } 
  updateStatus(id: string, status: string){
    return this.http.get(environment.apiUrl+'/placement-category/update-status/'+id+"/"+status);
  }
}
