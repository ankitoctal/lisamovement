import { TestBed } from '@angular/core/testing';

import { PlacementCategoryService } from './placement-category.service';

describe('PlacementCategoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlacementCategoryService = TestBed.get(PlacementCategoryService);
    expect(service).toBeTruthy();
  });
});
