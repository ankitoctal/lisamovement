import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BuildingService {

  constructor(private http: HttpClient) { }

  add(details:any) { 
		return this.http.post(environment.apiUrl+'/building-manager/add',details);   		
  } 
  edit(details:any){
    return this.http.post(environment.apiUrl+'/building-manager/edit',details);
  }
  delete(id:any){
    return this.http.get(environment.apiUrl+'/building-manager/delete/'+id);
  }
  getDetails() { 
    return this.http.get(environment.apiUrl+'/building-manager/get-details');   		
  } 
  view(id: string) {  
    return this.http.get(environment.apiUrl+'/building-manager/view/'+id);   		
  } 
  updateStatus(id: string,status: string){ 
    return this.http.get(environment.apiUrl+'/building-manager/update-status/'+id+"/"+status);
  }
}
