import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UserService {

	constructor(private http: HttpClient) { }	

    getTeamDropdown() { 
        return this.http.get(environment.apiUrl+'/team-list');          
    } 

	addUser(user) { 
		return this.http.post(environment.apiUrl+'/users/add',user);   		
    } 

    sendForgotPasswordMail(user) { 
        return this.http.post(environment.apiUrl+'/users/send_forgot_password_mail',user);           
    } 

    resetUserPassword(user){
        return this.http.post(environment.apiUrl+'/users/reset_user_password',user);       
    }

    
    addGuestUser(user) { 
      return this.http.post(environment.apiUrl+'/users/add_guest',user);           
    } 

    deleteUser(user_id){
      return this.http.get(environment.apiUrl+'/users/delete/'+user_id);
    }

    login(user) { 
		return this.http.post(environment.apiUrl+'/users/login',user);   		
    }  

    setUser (user) {
      localStorage.setItem('user', user); 
      return true;
    }

    getUser(user_id) { 
		return this.http.get(environment.apiUrl+'/users/getuser/'+user_id);   		
    } 

    getAllUser(role_id) { 
		return this.http.get(environment.apiUrl+'/users/getalluser/'+role_id);   		
    } 

    getUserPlan(user_id){
        return this.http.get(environment.apiUrl+'/users/get_user_plan/'+user_id);           
    }    

    saveUser(user) { 
		return this.http.post(environment.apiUrl+'/users/saveuser',user);   		
    }

    changePassword(user) { 
		return this.http.post(environment.apiUrl+'/users/changepassword',user);   		
    }

    userEdit(user_id){
        return this.http.get(environment.apiUrl+'/users/edit/'+user_id);
    }

}
