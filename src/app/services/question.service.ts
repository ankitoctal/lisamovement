import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private http: HttpClient) { }

  add(details:any) { 
		return this.http.post(environment.apiUrl+'/question-manager/add',details);   		
  } 
  addAnswer(details:any) { 
    return this.http.post(environment.apiUrl+'/answer-manager/add',details);      
  } 
  edit(details:any){
    return this.http.post(environment.apiUrl+'/question-manager/edit',details);
  }
  delete(id:string){
    return this.http.get(environment.apiUrl+'/question-manager/delete/'+id);
  }
  getDetails() { 
    return this.http.get(environment.apiUrl+'/question-manager/get-details');   		
  } 
  view(id: string) { 
    return this.http.get(environment.apiUrl+'/question-manager/view/'+id);   		
  } 
  updateStatus(id: string, status: string){
    return this.http.get(environment.apiUrl+'/question-manager/update-status/'+id+"/"+status);
  }
  getStages() { 
    return this.http.get(environment.apiUrl+'/stage-intro/get-details');   		
  } 
  getBuilding() { 
    return this.http.get(environment.apiUrl+'/building-manager/get-details');   		
  } 
  getIssue() { 
    return this.http.get(environment.apiUrl+'/inspector-messenger-manager/get-details');   		
  }
}
