import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ContractorService {

  constructor(private http: HttpClient) { }

  add(details) { 
		return this.http.post(environment.apiUrl+'/contractor-manager/add',details);   		
  } 
  edit(details){
    return this.http.post(environment.apiUrl+'/contractor-manager/edit',details);
  }
  delete(id){
    return this.http.get(environment.apiUrl+'/contractor-manager/delete/'+id);
  }
  getDetails() { 
    return this.http.get(environment.apiUrl+'/contractor-manager/get-details');   		
  } 
  view(id: string) { 
    return this.http.get(environment.apiUrl+'/contractor-manager/view/'+id);   		
  } 
  updateStatus(id: string,status: string){ console.log(id);console.log(status);
    return this.http.get(environment.apiUrl+'/contractor-manager/update-status/'+id+"/"+status);
  }
}
