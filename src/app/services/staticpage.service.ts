import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StaticpageService {

  constructor(private http: HttpClient) { }
  add(details) { 
		return this.http.post(environment.apiUrl+'/cms-manager/add',details);   		
  } 
  edit(details){
    return this.http.post(environment.apiUrl+'/cms-manager/edit',details);
  }
  delete(id){
    return this.http.get(environment.apiUrl+'/cms-manager/delete/'+id);
  }
  updateStatus(id,status){
    return this.http.get(environment.apiUrl+'/cms-manager/update-status/'+id+"/"+status);
  }
  view(id) { 
    return this.http.get(environment.apiUrl+'/cms-manager/view/'+id);   		
  } 

  getDetails() { 
    return this.http.get(environment.apiUrl+'/cms-manager/get-details');   		
  }

}
