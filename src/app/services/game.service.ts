import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private http: HttpClient) { }

  add(details:any) { 
		return this.http.post(environment.apiUrl+'/games/add',details);   		
  } 
  edit(details:any){
    return this.http.post(environment.apiUrl+'/games/edit',details);
  }

  getDetails() { 
    return this.http.get(environment.apiUrl+'/games/get-details');   		
  }

  view(id: string) { 
    return this.http.get(environment.apiUrl+'/games/view/'+id);   		
  }

  updateStatus(id: string, status: string){
    return this.http.get(environment.apiUrl+'/games/update-status/'+id+"/"+status);
  }

}
