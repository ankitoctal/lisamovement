import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StageService {

  constructor(private http: HttpClient) { }
 
  addStage(details) { 
    return this.http.post(environment.apiUrl+'/stage-intro/add',details);       
  }

  getStageDetails() { 
    return this.http.get(environment.apiUrl+'/stage-intro/get-details');      
  }

  viewStageDetail(id) { 
      return this.http.get(environment.apiUrl+'/stage-intro/view/'+id);       
  }

  editStageDetail(details){
    return this.http.post(environment.apiUrl+'/stage-intro/edit',details);
  }


  addResearch(details) { 
    return this.http.post(environment.apiUrl+'/stage-research/add',details);       
  }

  viewResearchDetail(id) { 
      return this.http.get(environment.apiUrl+'/stage-research/view/'+id);       
  }

  editResearchDetail(details){
    return this.http.post(environment.apiUrl+'/stage-research/edit',details);
  }

  getResearchDetails() { 
    return this.http.get(environment.apiUrl+'/stage-research/get-details');      
  }


}
