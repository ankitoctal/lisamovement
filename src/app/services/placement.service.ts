import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PlacementService {

  constructor(private http: HttpClient) { }

  add(details:any) { 
		return this.http.post(environment.apiUrl+'/placements-manager/add',details);   		
  } 
  edit(details:any){
    return this.http.post(environment.apiUrl+'/placements-manager/edit',details);
  }
  delete(id:string){
    return this.http.get(environment.apiUrl+'/placements-manager/delete/'+id);
  }
  getDetails() { 
    return this.http.get(environment.apiUrl+'/placements-manager/get-details');   		
  } 
  view(id: string) { 
    return this.http.get(environment.apiUrl+'/placements-manager/view/'+id);   		
  } 
  updateStatus(id: string, status: string){
    return this.http.get(environment.apiUrl+'/placements-manager/update-status/'+id+"/"+status);
  }
}
