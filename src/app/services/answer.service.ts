import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor(private http: HttpClient) { }

  add(details:any) { 
		return this.http.post(environment.apiUrl+'/answer-manager/add',details);   		
  } 
  edit(details:any){
    return this.http.post(environment.apiUrl+'/answer-manager/edit',details);
  }
  delete(id:string){
    return this.http.get(environment.apiUrl+'/answer-manager/delete/'+id);
  }
  getDetails() { 
    return this.http.get(environment.apiUrl+'/answer-manager/get-details');   		
  } 
  view(id: string) { 
    return this.http.get(environment.apiUrl+'/answer-manager/view/'+id);   		
  } 
  
}
