import { TestBed, inject } from '@angular/core/testing';

import { StaticpageService } from './staticpage.service';

describe('StaticpageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StaticpageService]
    });
  });

  it('should be created', inject([StaticpageService], (service: StaticpageService) => {
    expect(service).toBeTruthy();
  }));
});
