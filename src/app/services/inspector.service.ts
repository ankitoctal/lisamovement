import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class InspectorService {

  constructor(private http: HttpClient) { }

  add(details) { 
		return this.http.post(environment.apiUrl+'/inspector-messenger-manager/add',details);   		
  } 
  edit(details){
    return this.http.post(environment.apiUrl+'/inspector-messenger-manager/edit',details);
  }
  delete(id){
    return this.http.get(environment.apiUrl+'/inspector-messenger-manager/delete/'+id);
  }
  getDetails() { 
    return this.http.get(environment.apiUrl+'/inspector-messenger-manager/get-details');   		
  } 
  view(id: string) { 
    return this.http.get(environment.apiUrl+'/inspector-messenger-manager/view/'+id);   		
  } 
  updateStatus(id: string,status: string){
    return this.http.get(environment.apiUrl+'/inspector-messenger-manager/update-status/'+id+"/"+status);
  }
}
