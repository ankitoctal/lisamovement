import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
const headers = { 'Content-Type': 'multipart/form-data'}

@Injectable({
  providedIn: 'root'
})
export class CommonService {

	constructor(private http: HttpClient) { }

	add(details) { 
		return this.http.post(environment.apiUrl+'/logo-manager/add',details);   		
	} 
	edit(details){
		return this.http.post(environment.apiUrl+'/logo-manager/edit',details);
	}
	delete(id){
		return this.http.get(environment.apiUrl+'/logo-manager/delete/'+id);
	}
	updateStatus(id,status){
		return this.http.get(environment.apiUrl+'/logo-manager/update-status/'+id+"/"+status);
	}
	view(id) { 
		return this.http.get(environment.apiUrl+'/logo-manager/view/'+id);   		
	} 
	getDetails() { 
		return this.http.get(environment.apiUrl+'/logo-manager/get-details');   		
	} 
	getSettingDetails() { 
		return this.http.get(environment.apiUrl+'/settings/game-settings');   		
	} 
	saveGameSetting(data) { 
		return this.http.post(environment.apiUrl+'/settings/save-game-settings',data);   		
	}
	getGameVideosDetails() { 
		return this.http.get(environment.apiUrl+'/game-videos');   		
	} 
	addGameVideo(details) { 
		return this.http.post(environment.apiUrl+'/game-videos/add',details);   		
	}
	editGameVideo(details){
		return this.http.post(environment.apiUrl+'/game-videos/edit',details);
	}
	deleteGameVideo(id){
		return this.http.get(environment.apiUrl+'/game-videos/delete/'+id);
	}

	updateStatusGameVideo(id,status){
		return this.http.get(environment.apiUrl+'/game-videos/update-status/'+id+"/"+status);
	}

	viewGameVideo(id) { 
		return this.http.get(environment.apiUrl+'/game-videos/view/'+id);   		
	}


  	addSponser(details){
  		return this.http.post(environment.apiUrl+'/sponser/add',details); 
  	}

  	getSponsers(){
  		return this.http.get(environment.apiUrl+'/sponsers');	
  	}

    deleteSponser(id){
    	return this.http.get(environment.apiUrl+'/sponser/delete/'+id);
    }

    updateSponserStatus(id,status){
		return this.http.get(environment.apiUrl+'/sponser/update-status/'+id+"/"+status);
	}

	viewSponser(id) {
		return this.http.get(environment.apiUrl+'/sponser/view/'+id);
	}

	editSponser(details){
		return this.http.post(environment.apiUrl+'/sponser/edit',details);
	}

    
}
