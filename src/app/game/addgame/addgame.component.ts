import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { GameService } from '../../services/game.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-addgame',
  templateUrl: './addgame.component.html',
  styleUrls: ['./addgame.component.css']
})

export class AddgameComponent implements OnInit {

  dataForm:FormGroup;
  error ='';

  constructor(
    private _flashMessagesService: FlashMessagesService,
    private gameService: GameService,
    private router: Router, 
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.dataForm = new FormGroup({
        'title': new FormControl('',[Validators.required]),       
        'budget': new FormControl('',[Validators.required]),       
        'floor': new FormControl('',[Validators.required]),       
        'description': new FormControl(''),
        'saving_amount': new FormControl('')    
    });
  }
  
  public submitForm(){ 	
    var str = this.gameService.add(this.dataForm.value).subscribe(response => {       		
        if(response["status"] == 'true'){
          this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000});
          this.router.navigate(['/game']);	
          
        } else {
          this._flashMessagesService.show(response["msg"], { cssClass: 'alert-danger', timeout: 5000});
        }	
    });
     
  }

}

