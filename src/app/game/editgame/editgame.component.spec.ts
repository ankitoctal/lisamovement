import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditgamevideoComponent } from './editgamevideo.component';

describe('EditgamevideoComponent', () => {
  let component: EditgamevideoComponent;
  let fixture: ComponentFixture<EditgamevideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditgamevideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditgamevideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
