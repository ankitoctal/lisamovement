import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router";
import { GameService } from '../../services/game.service';

@Component({
  selector: 'app-editgame',
  templateUrl: './editgame.component.html',
  styleUrls: ['./editgame.component.css']
})
export class EditgameComponent implements OnInit {

  constructor(
    private gameService: GameService ,
    private router: Router, 
    private activatedRoute: ActivatedRoute,
    private _flashMessagesService: FlashMessagesService) { }

  userForm: any = {};

  id: any = {};

  ngOnInit() {

    this.id = this.activatedRoute.snapshot.paramMap.get('id');	   		
    this.gameService.view(this.id).subscribe((response:any) => { 
      this.userForm = response.data;  
    });
  }

  public submitForm(){
      this.gameService.edit(this.userForm).subscribe(response => {
          if(response["status"] == 'true'){    		
              this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
            
           } else {
              this._flashMessagesService.show(response["msg"], { cssClass: 'alert-danger', timeout: 5000 });
          }
          this.router.navigate(['/game']);
      });

      	
  }

}
