import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamevideoComponent } from './gamevideo.component';

describe('GamevideoComponent', () => {
  let component: GamevideoComponent;
  let fixture: ComponentFixture<GamevideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamevideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamevideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
