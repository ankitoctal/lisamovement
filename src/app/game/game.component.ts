import { Component, OnInit,ViewChild } from '@angular/core';
import { GameService } from '../services/game.service';
import { Router,ActivatedRoute } from "@angular/router"
import { FlashMessagesService } from 'angular2-flash-messages';
import * as $ from 'jquery';
import 'datatables.net';
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  
  user_id: any = {}; 
  public details: any = [];
  public data : any = [];
  
  constructor(
    private gameService: GameService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _flashMessagesService: FlashMessagesService) { 
    }

  @ViewChild("dataTable") table: any;
  tableWidget: any;
  dataTable: any;

  ngAfterViewInit() {
    this.initDatatable();
  }

  private initDatatable(): void {
    let exampleId: any = $('#example');
    this.tableWidget = exampleId.DataTable({
      select: true,
    });
  }


  updateStatus(id,status){
    this.gameService.updateStatus(id,status).subscribe((response: any) => {
      if(response["status"] == 'true'){
        location.reload(true);
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
      }                      
    });
  }
  
  ngOnInit() { 
    this.activatedRoute.params.subscribe(routeParams => {
      this.gameService.getDetails().subscribe((response: any) => {
        this.details = response.data;                    
      });
    });
    this.dataTable = $(this.table.nativeElement);
  }
}
