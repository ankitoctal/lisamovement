import {DomSanitizer} from '@angular/platform-browser';
import {PipeTransform, Pipe} from "@angular/core";

@Pipe({ name: 'safeUrl' })
export class SafeUrlPipe implements PipeTransform {
	saafUrl;
	constructor(private sanitizer: DomSanitizer) {}
	transform(url) { 
		this.saafUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
		return this.saafUrl;
	}
}