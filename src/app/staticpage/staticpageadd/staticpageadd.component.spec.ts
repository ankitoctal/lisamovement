import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticpageaddComponent } from './staticpageadd.component';

describe('StaticpageaddComponent', () => {
  let component: StaticpageaddComponent;
  let fixture: ComponentFixture<StaticpageaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticpageaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticpageaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
