import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { StaticpageService } from '../../services/staticpage.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
import { Title}  from '@angular/platform-browser';
//import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
@Component({
  selector: 'app-staticpageadd',  
  templateUrl: './staticpageadd.component.html',
  styleUrls: ['./staticpageadd.component.css'],
})
export class StaticpageaddComponent implements OnInit {
  userForm: FormGroup; 
  user_role :any = {};
  error ='';
  editor = ClassicEditor;
  data: any = `<p>Hello, world!</p>`;
  constructor(private staticpageService: StaticpageService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router,private titleService: Title) { }

  ngOnInit() {
    this.user_role = this.activatedRoute.snapshot.paramMap.get('role_id');;
    this.titleService.setTitle(environment.siteName + ' - Cms-Manager');
    this.userForm = new FormGroup({
        'page_name': new FormControl('', [
          Validators.required,        
        ]),
        'page_title': new FormControl('', [
          Validators.required        
        ]),
        'page_description': new FormControl('', [
          Validators.required,       
        ]),        
        'meta_title': new FormControl('', [
          Validators.required      
        ]),
        'meta_description': new FormControl('',[
          Validators.required,
          Validators.minLength(6),        
        ]), 
        'meta_keywords': new FormControl('',[
          Validators.required,      		        
        ]) 		
    });
  }
  public submitForm(){ 	
    this.user_role = this.activatedRoute.snapshot.paramMap.get('role_id');
    var str = this.staticpageService.add(this.userForm.value).subscribe(response => {       		
         if(response["status"] == 'true')
         { 
          this._flashMessagesService.show('Cms has been successfully added.', { cssClass: 'alert-success', timeout: 5000 });
          this.router.navigate(['/cms-manager']);	
          
      }
      else
      {
        this.error = response["msg"];
      }	
    });
     
  }

}
