import { Component,ViewChild, OnInit,ChangeDetectorRef} from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { StaticpageService } from '../services/staticpage.service';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-staticpage',
  templateUrl: './staticpage.component.html',
  styleUrls: ['./staticpage.component.css']
})
export class StaticpageComponent implements OnInit {
  @ViewChild("myckeditor") ckeditor: any;
  // CK editor configuration finsh here
  id: any = {}; 
  dataTable: any;
  public details : any;
  constructor(private staticPageService: StaticpageService,private router: Router,private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService,private chRef: ChangeDetectorRef) { 
  }
  @ViewChild('dataTable') table;
  tableWidget: any;
  delete(id){
    this.staticPageService.delete(id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show('Static Page has been deleted successfully', { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      }                      
    });
  }
  updateStatus(id,status){
    this.staticPageService.updateStatus(id,status).subscribe((response: any) => {
      if(response["status"] == 'true'){
        location.reload(true);
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
      }                      
    });
  }
  ngOnInit() { 
    
    this.activatedRoute.params.subscribe(routeParams => {
      this.staticPageService.getDetails().subscribe((response: any) => {
        this.details = response.data;     
        this.chRef.detectChanges();
        const table: any = $('table');
        this.dataTable = table.DataTable();               
      });
    });
  }

}
