import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { Title}  from '@angular/platform-browser';
import { StaticpageService } from '../../services/staticpage.service';
import { environment } from '../../../environments/environment';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
@Component({
  selector: 'app-staticpageedit',
  templateUrl: './staticpageedit.component.html',
  styleUrls: ['./staticpageedit.component.css']
})
export class StaticpageeditComponent implements OnInit {
  editor = ClassicEditor;
  data: any = `<p>Hello, world!</p>`;
  constructor(private staticPageService: StaticpageService , private router: Router, private activatedRoute: ActivatedRoute,private titleService: Title,private _flashMessagesService: FlashMessagesService) {
  
  }
  userForm: any = {};  	
  id: any = {}; 
  ngOnInit() {
    this.titleService.setTitle(environment.siteName + ' - Edit Cms');	
    this.id = this.activatedRoute.snapshot.paramMap.get('id');	   		
    this.staticPageService.view(this.id).subscribe((response:any) => { 
      this.userForm = response.data;	  		
    });
  }
  public submitForm(){
      this.staticPageService.edit(this.userForm).subscribe(response => {       		
      this._flashMessagesService.show('Cms Updated Successfully', { cssClass: 'alert-success', timeout: 5000 });
    });
    this.router.navigate(['/cms-manager']);	
  }

}
