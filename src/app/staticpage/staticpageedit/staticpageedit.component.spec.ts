import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticpageeditComponent } from './staticpageedit.component';

describe('StaticpageeditComponent', () => {
  let component: StaticpageeditComponent;
  let fixture: ComponentFixture<StaticpageeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticpageeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticpageeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
