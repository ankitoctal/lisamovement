import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticpageviewComponent } from './staticpageview.component';

describe('StaticpageviewComponent', () => {
  let component: StaticpageviewComponent;
  let fixture: ComponentFixture<StaticpageviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticpageviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticpageviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
