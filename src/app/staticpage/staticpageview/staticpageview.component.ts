import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router,ActivatedRoute } from "@angular/router";
import { StaticpageService } from '../../services/staticpage.service';
@Component({
  selector: 'app-staticpageview',
  templateUrl: './staticpageview.component.html',
  styleUrls: ['./staticpageview.component.css']
})
export class StaticpageviewComponent implements OnInit {

  constructor(private staticPageService: StaticpageService ,private userService: UserService,private activatedRoute: ActivatedRoute) { }
  id: any = {}; 
  data: any = {}; 
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');	
    this.staticPageService.view(this.id).subscribe((response:any) => { 
      this.data = response.data;	  		  
    });
  }

}
