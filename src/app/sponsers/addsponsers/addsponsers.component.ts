import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-addsponsers',
  templateUrl: './addsponsers.component.html',
  styleUrls: ['./addsponsers.component.css']
})

export class AddsponsersComponent implements OnInit {

	dataForm: FormGroup; 
	error ='';
	fileToUpload :any;
	sendFilevalue :any;
	image: File;
	resData: any;
	selectedFile = null;
	constructor(private commonService: CommonService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) { }

	ngOnInit() {
	    this.dataForm = new FormGroup({
	        'stage': new FormControl('', [ Validators.required]),
	        'name': new FormControl('', [ Validators.required]),
	       	'file': new FormControl('', [Validators.required]),
	    });
	}
	get f(){
		return this.dataForm.controls;
	}

	onFileChange(event) {
		if (event.target.files.length > 0) {
			this.fileToUpload  = event.target.files[0];
		}
	}

	public submitForm(){ 	
		const form = new FormData();
		form.append('stage', this.dataForm.value.stage);
		form.append('name', this.dataForm.value.name);
		form.append('image', this.fileToUpload, this.fileToUpload.name);

		var str = this.commonService.addSponser(form).subscribe(response => {       		
			if(response["status"] == 'true') {
				this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
				this.router.navigate(['/sponsers']);	
			} else {
				this.error = response["msg"];
			}	
		});
	}
}
