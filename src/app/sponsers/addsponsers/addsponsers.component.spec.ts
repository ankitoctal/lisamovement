import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsponsersComponent } from './addlogo.component';

describe('AddsponsersComponent', () => {
  let component: AddsponsersComponent;
  let fixture: ComponentFixture<AddsponsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsponsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsponsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
