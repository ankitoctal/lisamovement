import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-editsponsers',
  templateUrl: './editsponsers.component.html',
  styleUrls: ['./editsponsers.component.css']
})

export class EditsponsersComponent implements OnInit {

	fileToUpload :any;

  	editForm:FormGroup;
	userForm: any = {};
	id: any = {};
	constructor(
		private commonService: CommonService , 
		private router: Router, 
		private activatedRoute: ActivatedRoute,
		private _flashMessagesService: FlashMessagesService
	) { }
		

	ngOnInit() {
		this.id = this.activatedRoute.snapshot.paramMap.get('id');

		this.editForm = new FormGroup({
	        '_id': new FormControl(''),
	        'name': new FormControl('',[ Validators.required, Validators.minLength(3) ]),
	        'stage': new FormControl('',[ Validators.required]),
	        'file': new FormControl(''),
	    });

		this.commonService.viewSponser(this.id).subscribe((response:any) => {
			this.editForm.setValue({
		        '_id':response.data._id,
		        'name':response.data.name,
		        'stage':response.data.stage,
		        'file':''
		    });
		});
	}

	get f(){
		return this.editForm.controls;
	}

	onFileChange(event) {
		if (event.target.files.length > 0) {
			this.fileToUpload  = event.target.files[0];
		}
	}

	public submitForm(){

		const form = new FormData();
		form.append('_id', this.editForm.value._id);
		form.append('stage', this.editForm.value.stage);
		form.append('name', this.editForm.value.name);
		if (this.fileToUpload) {
			form.append('image', this.fileToUpload, this.fileToUpload.name);	
		}

		console.log(form);

		this.commonService.editSponser(form).subscribe(response => {       		
			this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
		});
		this.router.navigate(['/sponsers']);	
	}
}
