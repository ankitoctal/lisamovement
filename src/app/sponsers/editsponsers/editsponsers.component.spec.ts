import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditsponsersComponent } from './editsponsers.component';

describe('EditsponsersComponent', () => {
  let component: EditsponsersComponent;
  let fixture: ComponentFixture<EditsponsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditsponsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditsponsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
