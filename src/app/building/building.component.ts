import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { BuildingService } from '../services/building.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-building',
  templateUrl: './building.component.html',
  styleUrls: ['./building.component.css']
})
export class BuildingComponent implements OnInit {

  public details : any =[];
  constructor(private buildingService: BuildingService,private router: Router,private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService) { }
  @ViewChild("dataTable") table: any;
  tableWidget: any;
  dataTable: any;

  ngAfterViewInit() {
    this.initDatatable();
  }

  private initDatatable(): void {
    let exampleId: any = $('#example');
    this.tableWidget = exampleId.DataTable({
      select: true,
    });
  }
  delete(id: any){
    this.buildingService.delete(id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      }
    });
  }
  ngOnInit() {
    this.activatedRoute.params.subscribe(routeParams => {
      this.buildingService.getDetails().subscribe((response: any) => {
        this.details = response.data;    
      });
    });
    this.dataTable = $(this.table.nativeElement);
  }
  updateStatus(id: string,status: string){
    this.buildingService.updateStatus(id,status).subscribe((response: any) => {
      if(response["status"] == 'true'){
        location.reload(true);
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
      }                      
    });
  }

}
