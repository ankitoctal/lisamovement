import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {BuildingService} from '../../services/building.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
@Component({
  selector: 'app-addbuilding',
  templateUrl: './addbuilding.component.html',
  styleUrls: ['./addbuilding.component.css']
})
export class AddbuildingComponent implements OnInit {
  dataForm: FormGroup; 
  error ='';
  fileToUpload :any;
  editor = ClassicEditor;
  data: any = `<p>Hello, world!</p>`;
  constructor(private buildingService: BuildingService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) {  }

  ngOnInit() {
    this.dataForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required        
      ]),
      'file': new FormControl(''),
      'description': new FormControl('',[])
  });
  }
  get f(){
    return this.dataForm.controls;
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.fileToUpload  = event.target.files[0];
    }
  }
  public submitForm(){ 	
    const formData = new FormData();
    formData.append('name', this.dataForm.value.name);
    formData.append('image', this.fileToUpload, this.fileToUpload.name);
    formData.append('description', this.dataForm.value.description);
    var str = this.buildingService.add(formData).subscribe(response => {       		
      if(response["status"] == 'true')
      { 
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        this.router.navigate(['/building-manager']);	
      }
      else
      {
        this.error = response["msg"];
      }	
    });
     
  }

}
