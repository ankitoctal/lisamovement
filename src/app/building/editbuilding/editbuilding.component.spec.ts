import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditbuildingComponent } from './editbuilding.component';

describe('EditbuildingComponent', () => {
  let component: EditbuildingComponent;
  let fixture: ComponentFixture<EditbuildingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditbuildingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditbuildingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
