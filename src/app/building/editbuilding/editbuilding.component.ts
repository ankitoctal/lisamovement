import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import {BuildingService} from '../../services/building.service';
import { environment } from '../../../environments/environment';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
@Component({
  selector: 'app-editbuilding',
  templateUrl: './editbuilding.component.html',
  styleUrls: ['./editbuilding.component.css']
})
export class EditbuildingComponent implements OnInit {

  dataForm: any = {}; 	
  id: string; 
  env = environment;
  fileToUpload :any;
  editor = ClassicEditor;
  data: any;
  constructor(private buildingService: BuildingService,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.activatedRoute.params.subscribe(routeParams => {
      this.buildingService.view(routeParams.id).subscribe((response: any) => {
        this.dataForm = response.data; 
      });
    });
  }
  get f(){
    return this.dataForm.controls;
  }
  onFileChange(event) { 
    if (event.target.files.length > 0) {
      this.fileToUpload  = event.target.files[0];
    }
  }
  public submitForm(){
    const formData = new FormData();
    formData.append('name', this.dataForm.name);
    formData.append('_id', this.dataForm._id);
    if(this.fileToUpload != undefined){
      formData.append('image', this.fileToUpload, this.fileToUpload.name);
    }
    formData.append('description', this.dataForm.description);
      this.buildingService.edit(formData).subscribe(response => {       		
      this._flashMessagesService.show(response['msg'], { cssClass: 'alert-success', timeout: 5000 });
    });
    this.router.navigate(['/building-manager']);	
  }

}
