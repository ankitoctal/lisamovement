import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {QuestionService} from '../../services/question.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
import { forkJoin } from 'rxjs';
@Component({
  selector: 'app-addquestion',
  templateUrl: './addquestion.component.html',
  styleUrls: ['./addquestion.component.css']
})
export class AddquestionComponent implements OnInit {
  dataForm: FormGroup; 
  error ='';
  fileToUpload :any;
  stageList: {};
  buildingList: {};
  issueList: {};
  constructor(private questionService: QuestionService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) { 
   }

  	ngOnInit() {
  	/*
	    const buildingList2 = this.questionService.getBuilding();
	    forkJoin([buildingList2]).subscribe(results => {
	      this.buildingList = results[0]['data'];
	    });*/

	    this.dataForm = new FormGroup({
			'title': new FormControl('', [ Validators.required]),
			'question': new FormControl('', [ Validators.required]),
			'stage': new FormControl('', [Validators.required])
	  	});
  	}

  	get f(){
    	return this.dataForm.controls;
  	}


  	public submitForm(){ 	

	    var str = this.questionService.add( this.dataForm.value).subscribe(response => {
	     	if(response["status"] == 'true')
	     	{ 
	    		this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
	        	this.router.navigate(['/question-manager']);
	      	}
	      	else
	      	{
	        	this.error = response["msg"];
	      	}
	    });
	}

}
