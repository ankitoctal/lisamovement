import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import {QuestionService} from '../../services/question.service';
import { environment } from '../../../environments/environment';
import { forkJoin } from 'rxjs';
@Component({
  selector: 'app-editquestion',
  templateUrl: './editquestion.component.html',
  styleUrls: ['./editquestion.component.css']
})
export class EditquestionComponent implements OnInit {
  dataForm: any = {}; 	
  id: string; 
  env = environment;
  fileToUpload :any;
  stageList: {};
  buildingList: {};
  issueList: {};
  constructor(private questionService: QuestionService,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) { }

  ngOnInit() {
    /*
    const buildingList2 = this.questionService.getBuilding();
    forkJoin([buildingList2]).subscribe(results => {
      this.buildingList = results[0]['data'];
    });*/

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.activatedRoute.params.subscribe(routeParams => {
      this.questionService.view(routeParams.id).subscribe((response: any) => {
        this.dataForm = response.data; 
        
      });
    });
  }

  get f(){
    return this.dataForm.controls;
  }
 
  public submitForm(){
 
    this.questionService.edit(this.dataForm).subscribe(response => {       		
    this._flashMessagesService.show(response['msg'], { cssClass: 'alert-success', timeout: 5000 });
    });
    this.router.navigate(['/question-manager']);	
  }

}
