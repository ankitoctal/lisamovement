import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { QuestionService } from '../../services/question.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
import { forkJoin } from 'rxjs';
@Component({
  selector: 'app-addanswer',
  templateUrl: './addanswer.component.html',
  styleUrls: ['./addanswer.component.css']
})
export class AddanswerComponent implements OnInit {
  dataForm: FormGroup;
  id: string; 
  error ='';
  details :any;
  stageList: {};
  buildingList: {};
  issueList: {};
  constructor(private questionService: QuestionService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) { 
   }

  	ngOnInit() {

  		this.id = this.activatedRoute.snapshot.paramMap.get('id');
	    this.activatedRoute.params.subscribe(routeParams => {
		    this.questionService.view(routeParams.id).subscribe((response: any) => {
		        console.log(response.data);
		        this.details = response.data;
		        this.dataForm = new FormGroup({
					'answer': new FormControl('', [ Validators.required]),
					'question_id': new FormControl(this.details._id),
					'price': new FormControl('', [ Validators.required]),
					'choice': new FormControl('', [Validators.required])
			  	});
		    });
	    });
  	}

  	get f(){
    	return this.dataForm.controls;
  	}


  	public submitForm(){
  		console.log( this.dataForm.value);

  		var str = this.questionService.addAnswer( this.dataForm.value).subscribe(response => {
	     	if(response["status"] == 'true')
	     	{ 
	    		this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
	        	this.router.navigate(['/question-manager']);
	      	}
	      	else
	      	{
	        	this.error = response["msg"];
	      	}
	    });
	}

}
