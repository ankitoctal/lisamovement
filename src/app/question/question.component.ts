import { Component, OnInit,ChangeDetectorRef  } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { QuestionService } from '../services/question.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  dataTable: any;
  public details : any =[];
  constructor(private QuestionService: QuestionService,private router: Router,private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService,private chRef: ChangeDetectorRef) { }
  
  ngOnInit() {
    this.activatedRoute.params.subscribe(routeParams => {
      this.QuestionService.getDetails().subscribe((response: any) => {
      this.details = response.data;    
      this.chRef.detectChanges();
      const table: any = $('table');
      this.dataTable = table.DataTable();
      });
    });
  }

  delete(id: any){
    this.QuestionService.delete(id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      }
    });
  }
  updateStatus(id: string,status: string){
    this.QuestionService.updateStatus(id,status).subscribe((response: any) => {
      if(response["status"] == 'true'){
        location.reload(true);
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
      }                      
    });
  }

}
