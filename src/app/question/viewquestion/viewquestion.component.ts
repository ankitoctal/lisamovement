import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../../services/question.service';
import { Router, ActivatedRoute } from "@angular/router"
@Component({
  selector: 'app-viewquestion',
  templateUrl: './viewquestion.component.html',
  styleUrls: []
})
export class ViewquestionComponent implements OnInit {
  
	constructor(
		private questionService: QuestionService,
		private activatedRoute: ActivatedRoute
	) { }
  
	public user: any = [];
	id: any; 
	data: any = {}; 
	answers: any = {}; 
	
	ngOnInit() {

    	this.id = this.activatedRoute.snapshot.paramMap.get('id');

    	this.questionService.view(this.id).subscribe((response:any) => {
    		console.log(response); 
	    	this.data = response.data;	  		  
	    	this.answers = response.answers;	  		  
	    });
  	}

}
