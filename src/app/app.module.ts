import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent }  from './header/header.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { UserindexComponent } from './user/userindex/userindex.component';
import { UseraddComponent } from './user/useradd/useradd.component';
import { UsereditComponent } from './user/useredit/useredit.component';
import { UserviewComponent } from './user/userview/userview.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';  
import { NgDatepickerModule } from 'ng2-datepicker';
import { SafeUrlPipe } from './pipe/safeurl.pipe';

/*
* servies
*/
import { AuthGuardService } from './services/auth-guard.service';
import { LogoutComponent } from './logout/logout.component';
import { UserprofileComponent } from './user/userprofile/userprofile.component';
import { ChangepasswordComponent } from './user/changepassword/changepassword.component';
import { StaticpageComponent } from './staticpage/staticpage.component';
import { StaticpageaddComponent } from './staticpage/staticpageadd/staticpageadd.component';
import { StaticpageeditComponent } from './staticpage/staticpageedit/staticpageedit.component';
import { StaticpageviewComponent } from './staticpage/staticpageview/staticpageview.component';
import { LogoComponent } from './logo/logo.component';
import { AddlogoComponent } from './logo/addlogo/addlogo.component';
import { EditlogoComponent } from './logo/editlogo/editlogo.component';
import { SettingsComponent } from './settings/settings.component';
import { GamevideoComponent } from './gamevideo/gamevideo.component';
import { EditgamevideoComponent } from './gamevideo/editgamevideo/editgamevideo.component';
import { AddgamevideoComponent } from './gamevideo/addgamevideo/addgamevideo.component';
import { ContentmanagerComponent } from './contentmanager/contentmanager.component';
import { AddcontentmanagerComponent } from './contentmanager/addcontentmanager/addcontentmanager.component';
import { EditcontentmanagerComponent } from './contentmanager/editcontentmanager/editcontentmanager.component';

import { StageComponent} from './stage/intro/stage.component';
import { StageaddComponent} from './stage/intro/stageadd/stageadd.component';
import { StageeditComponent} from './stage/intro/stageedit/stageedit.component';
import { StageviewComponent} from './stage/intro/stageview/stageview.component';

import { ResearchComponent} from './stage/research/research.component';
import { ResearchaddComponent} from './stage/research/researchadd/researchadd.component';
import { ResearcheditComponent} from './stage/research/researchedit/researchedit.component';
import { ResearchviewComponent} from './stage/research/researchview/researchview.component';

import { GameComponent } from './game/game.component';
import { AddgameComponent } from './game/addgame/addgame.component';
import { EditgameComponent } from './game/editgame/editgame.component';

import { ContractorComponent } from './contractor/contractor.component';
import { AddcontractorComponent } from './contractor/addcontractor/addcontractor.component';
import { EditcontractorComponent } from './contractor/editcontractor/editcontractor.component';
import { BuildingComponent } from './building/building.component';
import { AddbuildingComponent } from './building/addbuilding/addbuilding.component';
import { EditbuildingComponent } from './building/editbuilding/editbuilding.component';
import { InspectorComponent } from './inspector/inspector.component';
import { AddinspectorComponent } from './inspector/addinspector/addinspector.component';
import { EditinspectorComponent } from './inspector/editinspector/editinspector.component';
import { SafeHtmlPipe } from './pipe/safe-html.pipe';
import { QuestionComponent } from './question/question.component';
import { AddquestionComponent } from './question/addquestion/addquestion.component';
import { ViewquestionComponent } from './question/viewquestion/viewquestion.component';
import { EditquestionComponent } from './question/editquestion/editquestion.component';

import { AddanswerComponent } from './question/addanswer/addanswer.component';

import { EmailTemplateComponent} from './emailtemplate/emailtemplate.component';
import { EmailTemplateaddComponent} from './emailtemplate/emailtemplateadd/emailtemplateadd.component';
import { EmailTemplateeditComponent} from './emailtemplate/emailtemplateedit/emailtemplateedit.component';
import { EmailTemplateviewComponent} from './emailtemplate/emailtemplateview/emailtemplateview.component';
import { PlacementsComponent } from './placements/placements.component';
import { AddplacementComponent } from './placements/addplacement/addplacement.component';
import { EditplacementComponent } from './placements/editplacement/editplacement.component';
import { ViewplacementComponent } from './placements/viewplacement/viewplacement.component';
import { PlacementcategoryComponent } from './placementcategory/placementcategory.component';
import { AddplacementcategoryComponent } from './placementcategory/addplacementcategory/addplacementcategory.component';
import { EditplacementcategoryComponent } from './placementcategory/editplacementcategory/editplacementcategory.component';

import { SponsersComponent } from './sponsers/sponsers.component';
import { EditsponsersComponent } from './sponsers/editsponsers/editsponsers.component';
import { AddsponsersComponent } from './sponsers/addsponsers/addsponsers.component';

@NgModule({
  declarations: [
    AppComponent, LoginComponent, HeaderComponent, UserindexComponent, UseraddComponent, UsereditComponent, UserviewComponent, SidebarComponent, FooterComponent, LogoutComponent, UserprofileComponent, ChangepasswordComponent, StaticpageComponent, StaticpageaddComponent, StaticpageeditComponent, StaticpageviewComponent, LogoComponent, AddlogoComponent, EditlogoComponent, SettingsComponent, GamevideoComponent, EditgamevideoComponent, AddgamevideoComponent,SafeUrlPipe, ContentmanagerComponent, AddcontentmanagerComponent, EditcontentmanagerComponent,StageComponent,StageaddComponent,StageeditComponent,StageviewComponent, ContractorComponent, AddcontractorComponent, EditcontractorComponent, BuildingComponent, AddbuildingComponent, EditbuildingComponent, InspectorComponent, AddinspectorComponent, EditinspectorComponent,SafeHtmlPipe, QuestionComponent, AddquestionComponent, EditquestionComponent,EmailTemplateComponent, EmailTemplateaddComponent, EmailTemplateeditComponent, EmailTemplateviewComponent, ResearchComponent,ResearchaddComponent, ResearcheditComponent, ResearchviewComponent, GameComponent, AddgameComponent, EditgameComponent, PlacementsComponent, AddplacementComponent, EditplacementComponent, ViewplacementComponent, PlacementcategoryComponent, AddplacementcategoryComponent, EditplacementcategoryComponent, AddanswerComponent,ViewquestionComponent,SponsersComponent,EditsponsersComponent,AddsponsersComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule,FormsModule,ReactiveFormsModule,HttpClientModule,BrowserAnimationsModule,CKEditorModule,NgDatepickerModule,FlashMessagesModule.forRoot()
  ],
  providers: [AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
