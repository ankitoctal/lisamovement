import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  constructor(private userService: UserService,private _flashMessagesService: FlashMessagesService,public router: Router) { }
  userForm: any = {};
  public currentUser: any = {};
  ngOnInit() {	
    this.currentUser = JSON.parse(localStorage.getItem('token'));
    this.userService.getUser(this.currentUser.id).subscribe((response:any) => { 
      this.userForm = response.data;	  
    });	 
  }
  public submitForm(){
    this.userService.saveUser(this.userForm).subscribe(response => {       		
    this._flashMessagesService.show('User Updated Successfully', { cssClass: 'alert-success', timeout: 5000 });
     this.router.navigate(['/myprofile']);
   });
  }
}
