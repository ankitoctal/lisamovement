import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { UserService } from '../../services/user.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
import { Title}  from '@angular/platform-browser';
@Component({
  selector: 'app-useradd',
  templateUrl: './useradd.component.html',
  styleUrls: ['./useradd.component.css']
})
export class UseraddComponent implements OnInit {

  userForm: FormGroup; 
  user_role :any = {};
  error ='';
  teamList = [];

  constructor(private userService: UserService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router,private titleService: Title) { }

  ngOnInit() {  
    this.userService.getTeamDropdown().subscribe((response: any) => {
          this.teamList = response.data;    
        });
    
    this.user_role = this.activatedRoute.snapshot.paramMap.get('role_id');;
    this.titleService.setTitle(environment.siteName + ' - Register');
    this.userForm = new FormGroup({
        'first_name': new FormControl('', [
          Validators.required,
          Validators.minLength(4),        
        ]),
        'last_name': new FormControl('', [
          Validators.required,
          Validators.minLength(4),        
        ]),
        'email': new FormControl('',[
          Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),        
        'team_name': new FormControl(''),
        'position': new FormControl(''),
        'team_id': new FormControl(''),
        'password': new FormControl('',[
          Validators.required,
          Validators.minLength(6),        
        ]), 
        'confirm_password': new FormControl('',[
          Validators.minLength(1),        		        
        ]), 
        'role_id': new FormControl('')  		
    },ValidateConfirmPassword);
  }

  public submitForm(){ 	
    this.user_role = this.activatedRoute.snapshot.paramMap.get('role_id');
    this.userForm.patchValue({    
      "role_id":this.user_role     
      }); 

    var str = this.userService.addUser(this.userForm.value).subscribe(response => {       		
         if(response["status"] == 'true')
         { 
          this._flashMessagesService.show('User has been successfully added.', { cssClass: 'alert-success', timeout: 5000 });
          this.router.navigate(['/users/'+this.user_role]);	
          
      }
      else
      {
        this.error = response["msg"];
      }	
    });
     
  }
  
  isEmailUnique(control: FormControl) {  		

    return this.http.get(environment.apiUrl+'/users/check_email/'+control.value);
  }

  reloadPage(){ 
    location.reload();
  }

}

export function ValidateConfirmPassword(findForm: FormControl) {

	if (findForm["controls"].password.value == findForm["controls"].confirm_password.value) {	
		return null;
	}
	return { validConfirmPassword: true };
}
