import { Component,ViewChild, OnInit,ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { UserService } from '../../services/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-userindex',
  templateUrl: './userindex.component.html',
  styleUrls: ['./userindex.component.css']
})
export class UserindexComponent implements OnInit {
  
  user_id: any = {}; 
  public user: any = [];
  public data : any = [];
  public role_id: any = {}; 
  dataTable: any;
  constructor(private UserService: UserService,private router: Router,private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService,private chRef: ChangeDetectorRef) {
  }

  delete(user_id){
    this.UserService.deleteUser(user_id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show('User has been deleted successfully', { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      } 
                      
    });
  }
  ngOnInit() { 
    this.activatedRoute.params.subscribe(routeParams => {
      this.role_id = routeParams.role_id;
      this.UserService.getAllUser(routeParams.role_id).subscribe((response: any) => {
        this.user = response.data;
        this.chRef.detectChanges();
        const table: any = $('table');
        this.dataTable = table.DataTable();
      });
    });
  }
}
