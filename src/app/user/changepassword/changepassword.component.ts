import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  public user;
  userForm: any = {};  
  constructor(public router: Router,private userService: UserService,private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
  		this.userForm = new FormGroup({
  			'id': new FormControl(''), 
  			'old_password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        
      		]),     		
      		'password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        
      		]), 
      		'confirm_password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        		        
      		]),     		
    	},ValidateConfirmPassword);	

      this.user = JSON.parse(localStorage.getItem('token'));	
      console.log(this.user);
  }
  public submitForm(){  		
    if (this.userForm.valid) {
       this.user = JSON.parse(localStorage.getItem('token'));	
        this.userForm.value.id = this.user.id;        	
          this.userService.changePassword(this.userForm.value).subscribe(response => { 
            if(response['status']=='success')      		
            {
              this._flashMessagesService.show('Password Changed Successfully', { cssClass: 'alert-success', timeout: 5000 });
              this.router.navigate(['change-password']);	
          }
          else
          {
            this._flashMessagesService.show(response['msg'], { cssClass: 'alert-error', timeout: 5000 });
          }	
        });
        } else {
          this.validateAllFormFields(this.userForm);
        }  
    }
    validateAllFormFields(formGroup: FormGroup) {
      Object.keys(formGroup.controls).forEach(field => {
        //console.log(field);
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
        }
      });
  }
}


export function ValidateConfirmPassword(findForm: FormControl) {

	if (findForm["controls"].password.value == findForm["controls"].confirm_password.value) {	
		return null;
	}
	return { validConfirmPassword: true };
}
