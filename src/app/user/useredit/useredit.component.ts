import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { UserService } from '../../services/user.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { Title}  from '@angular/platform-browser';
import {Location} from '@angular/common';
@Component({
  selector: 'app-useredit',
  templateUrl: './useredit.component.html',
  styleUrls: ['./useredit.component.css']
})
export class UsereditComponent implements OnInit {
  
  constructor(private userService: UserService , private router: Router, private activatedRoute: ActivatedRoute,private titleService: Title,private _flashMessagesService: FlashMessagesService,private _location: Location) {
  
  }
  env = environment;	
  userForm: any = {};
  user_id: any = {}; 
  ngOnInit() {
    this.titleService.setTitle(environment.siteName + ' - Edit User');	
    this.user_id = this.activatedRoute.snapshot.paramMap.get('user_id');	   		
    this.userService.getUser(this.user_id).subscribe((response:any) => { 
      this.userForm = response.data;	  		
    });	 
  }

  public submitForm(){
      this.userService.userEdit(this.userForm).subscribe(response => {       		
      this._flashMessagesService.show('User Updated Successfully', { cssClass: 'alert-success', timeout: 5000 });
    });
    this._location.back();
  }

}
