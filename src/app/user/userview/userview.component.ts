import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router,ActivatedRoute } from "@angular/router"
@Component({
  selector: 'app-userview',
  templateUrl: './userview.component.html',
  styleUrls: ['./userview.component.css']
})
export class UserviewComponent implements OnInit {
  
  constructor(private UserService: UserService,private userService: UserService,private activatedRoute: ActivatedRoute) { }
  public user: any = [];
  user_id: any = {}; 
  data: any = {}; 
  ngOnInit() {
    this.user_id = this.activatedRoute.snapshot.paramMap.get('user_id');	
    this.userService.getUser(this.user_id).subscribe((response:any) => { 
      this.data = response.data;	  		  
    });
  }

}
