import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { PlacementService } from '../services/placement.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-placements',
  templateUrl: './placements.component.html',
  styleUrls: ['./placements.component.css']
})
export class PlacementsComponent implements OnInit {

  public details : any =[];
  constructor(private placementService: PlacementService,private router: Router,private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService) { }
  @ViewChild("dataTable") table: any;
  tableWidget: any;
  dataTable: any;

  ngAfterViewInit() {
    this.initDatatable();
  }

  private initDatatable(): void {
    let exampleId: any = $('#example');
    this.tableWidget = exampleId.DataTable({
      select: true,
    });
  }
  delete(id: any){
    this.placementService.delete(id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      }
    });
  }
  ngOnInit() {
    this.activatedRoute.params.subscribe(routeParams => {
      this.placementService.getDetails().subscribe((response: any) => {
        this.details = response.data;    
        console.log(this.details);
      });
    });
    this.dataTable = $(this.table.nativeElement);
  }
  updateStatus(id: string,status: string){
    this.placementService.updateStatus(id,status).subscribe((response: any) => {
      if(response["status"] == 'true'){
        location.reload(true);
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
      }                      
    });
  }

}
