import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { PlacementService } from '../../services/placement.service';
import { PlacementCategoryService } from '../../services/placement-category.service';
@Component({
  selector: 'app-editplacement',
  templateUrl: './editplacement.component.html',
  styleUrls: ['./editplacement.component.css']
})
export class EditplacementComponent implements OnInit {

  constructor(private placementService: PlacementService , private router: Router,private placementCategoryService: PlacementCategoryService, private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService) { }
    userForm: any = {};  	
    id: any = {};
    categories: any;
    ngOnInit() {
      this.placementCategoryService.getDetails().subscribe((response: any) => {
        this.categories = response.data; 
      });
      this.id = this.activatedRoute.snapshot.paramMap.get('id');	   
      this.activatedRoute.params.subscribe(routeParams => {
        this.placementService.view(routeParams.id).subscribe((response: any) => {
          this.userForm = response.data; 
        });
      });
    }
    public submitForm(){
      this.placementService.edit(this.userForm).subscribe(response => {      
      this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
    });
    this.router.navigate(['/placements-manager']);	
  }

}
