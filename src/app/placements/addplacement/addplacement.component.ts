import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlacementService } from '../../services/placement.service';
import { PlacementCategoryService } from '../../services/placement-category.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
@Component({
  selector: 'app-addplacement',
  templateUrl: './addplacement.component.html',
  styleUrls: ['./addplacement.component.css']
})
export class AddplacementComponent implements OnInit {

  dataForm:FormGroup;
  error ='';
  categories: any;
  constructor(private _flashMessagesService: FlashMessagesService,private placementService: PlacementService,private placementCategoryService: PlacementCategoryService,private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.placementCategoryService.getDetails().subscribe((response: any) => {
      this.categories = response.data; 
    });
    this.dataForm = new FormGroup({
      'choice': new FormControl('',[
        Validators.required ]),        
      'answer': new FormControl('',[
        Validators.required      
      ]),        
      'category': new FormControl('',[
        Validators.required      
      ]) 		
  });
  }
  
  public submitForm(){ 	
    var str = this.placementService.add(this.dataForm.value).subscribe(response => {       		
         if(response["status"] == 'true')
         { 
          this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
          this.router.navigate(['/placements-manager']);	
          
      }
      else
      {
        this.error = response["msg"];
      }	
    });
     
  }

}
