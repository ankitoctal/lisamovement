import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { InspectorService } from '../services/inspector.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-inspector',
  templateUrl: './inspector.component.html',
  styleUrls: ['./inspector.component.css']
})
export class InspectorComponent implements OnInit {

  public details : any =[];
  constructor(private inspectorService: InspectorService,private router: Router,private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService) { }
  @ViewChild("dataTable") table: any;
  tableWidget: any;
  dataTable: any;

  ngAfterViewInit() {
    this.initDatatable();
  }

  private initDatatable(): void {
    let exampleId: any = $('#example');
    this.tableWidget = exampleId.DataTable({
      select: true,
    });
  }
  delete(id: any){
    this.inspectorService.delete(id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      }
    });
  }
  ngOnInit() {
    this.activatedRoute.params.subscribe(routeParams => {
      this.inspectorService.getDetails().subscribe((response: any) => {
        this.details = response.data;    
      });
    });
    this.dataTable = $(this.table.nativeElement);
  }
  updateStatus(id: string,status: string){
    this.inspectorService.updateStatus(id,status).subscribe((response: any) => {
      if(response["status"] == 'true'){
        location.reload(true);
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
      }                      
    });
  }

}
