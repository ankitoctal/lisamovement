import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import {InspectorService} from '../../services/inspector.service';
import { environment } from '../../../environments/environment';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-editinspector',
  templateUrl: './editinspector.component.html',
  styleUrls: ['./editinspector.component.css']
})
export class EditinspectorComponent implements OnInit {
  dataForm: any = {}; 	
  id: string; 
  env = environment;
  fileToUpload :any;
  editor = ClassicEditor;
  data: any;
  constructor(private inspectorService: InspectorService,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.activatedRoute.params.subscribe(routeParams => {
      this.inspectorService.view(routeParams.id).subscribe((response: any) => {
        this.dataForm = response.data; 
      });
    });
  }
  get f(){
    return this.dataForm.controls;
  }
  onFileChange(event:any) { 
    if (event.target.files.length > 0) {
      this.fileToUpload  = event.target.files[0];
    }
  }
  public submitForm(){
    const formData = new FormData();
    formData.append('_id', this.dataForm._id);
    formData.append('full_name', this.dataForm.full_name);
    formData.append('designation', this.dataForm.designation);
    formData.append('bio', this.dataForm.bio);
    if(this.fileToUpload != undefined){
      formData.append('image', this.fileToUpload, this.fileToUpload.name);
    }
      this.inspectorService.edit(formData).subscribe(response => {       		
      this._flashMessagesService.show(response['msg'], { cssClass: 'alert-success', timeout: 5000 });
    });
    this.router.navigate(['/inspector-messenger-manager']);	
  }

}
