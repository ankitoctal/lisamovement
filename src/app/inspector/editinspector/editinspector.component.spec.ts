import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditinspectorComponent } from './editinspector.component';

describe('EditinspectorComponent', () => {
  let component: EditinspectorComponent;
  let fixture: ComponentFixture<EditinspectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditinspectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditinspectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
