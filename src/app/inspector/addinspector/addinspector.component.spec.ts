import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddinspectorComponent } from './addinspector.component';

describe('AddinspectorComponent', () => {
  let component: AddinspectorComponent;
  let fixture: ComponentFixture<AddinspectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddinspectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddinspectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
