import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {InspectorService} from '../../services/inspector.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-addinspector',
  templateUrl: './addinspector.component.html',
  styleUrls: ['./addinspector.component.css']
})
export class AddinspectorComponent implements OnInit {
  dataForm: FormGroup; 
  error ='';
  fileToUpload :any;
  editor = ClassicEditor;
  data: any;
  constructor(private inspectorService: InspectorService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) {  }

  ngOnInit() {
    this.dataForm = new FormGroup({
      'full_name': new FormControl('', [
        Validators.required        
      ]),
      'designation': new FormControl('',[]),
      'file': new FormControl('',[]),
      'bio': new FormControl('',[])
  });
  }
  get f(){
    return this.dataForm.controls;
  }
  onFileChange(event:any) {
    if (event.target.files.length > 0) {
      this.fileToUpload  = event.target.files[0];
    }
  }
  public submitForm(){ 	
    const formData = new FormData();
    formData.append('full_name', this.dataForm.value.full_name);
    formData.append('image', this.fileToUpload, this.fileToUpload.name);
    formData.append('designation', this.dataForm.value.designation);
    formData.append('bio', this.dataForm.value.bio);
    var str = this.inspectorService.add(formData).subscribe(response => {       		
      if(response["status"] == 'true')
      { 
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        this.router.navigate(['/inspector-messenger-manager']);	
      }
      else
      {
        this.error = response["msg"];
      }	
    });
     
  }

}
