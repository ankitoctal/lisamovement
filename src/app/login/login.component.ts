import { Component, OnInit } from '@angular/core';
import {ViewEncapsulation} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../services/user.service';
import { Title}  from '@angular/platform-browser';
import * as jwt_decode from 'jwt-decode';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup; 
    error ='';
    constructor(
        private router: Router,
        private user:UserService,
        private titleService: Title,
        private _flashMessagesService: FlashMessagesService
    ) {
    }

    email= '';
    password = '';
    btnDisabled: boolean = false;
    public currentUser: any = [];
    public current_user: any = [];
    ngOnInit() {
        if (localStorage.getItem('token')) {
            this.router.navigate(['/users/2']);
        }

        this.titleService.setTitle(environment.siteName + ' - Login');
        this.loginForm = new FormGroup({
            'email': new FormControl('',[
              Validators.required,
              Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),        
            'password': new FormControl('',[
              Validators.required,
              Validators.minLength(6),        
            ])   		
        });
    }
    validate() {
        if(this.email) {
            if(this.password) {
                return true;
            } else {
               this._flashMessagesService.show('Password is required.', { cssClass: 'alert-success', timeout: 5000 });
            }
        } else {
            this._flashMessagesService.show('Email is required.', { cssClass: 'alert-success', timeout: 5000 });
        }
    }
    login() {
        try {
            var str = this.user.login(this.loginForm.value).subscribe((data:any) => {     
                if(data["success"] == true) {  
                   this.currentUser = jwt_decode(data['token']);
                   this.current_user = {
                    id: this.currentUser.user._id,
                    full_name: `${this.currentUser.user.first_name} ${this.currentUser.user.last_name}`,
                    first_name: this.currentUser.user.first_name,
                    last_name: this.currentUser.user.last_name,
                    email: this.currentUser.user.email, 
                    team_name: this.currentUser.user.team_name, 
                    status: this.currentUser.user.status, 
                    created_at: this.currentUser.user.status, 
                    updated_at: this.currentUser.user.updated_at};
                    //console.log(this.current_user.id);
                    localStorage.setItem('token',JSON.stringify(this.current_user));

                    this._flashMessagesService.show(data['message'], { cssClass: 'alert-success', timeout: 5000 });
                    location.reload(true);
                    //window.location.reload(); 
                    //this.router.navigate(['/users']);   
                          
                 }
                 else {  
                     this._flashMessagesService.show(data['message'], { cssClass: 'alert-danger', timeout: 5000 });
                 }	 
               });
        } catch(error) {
            this._flashMessagesService.show(error['message'], { cssClass: 'alert-success', timeout: 5000 });
        }
    }

}
