import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { UserindexComponent }  from './user/userindex/userindex.component';
import { UseraddComponent }  from './user/useradd/useradd.component';
import { UsereditComponent }  from './user/useredit/useredit.component';
import { UserviewComponent }  from './user/userview/userview.component';
import { UserprofileComponent }  from './user/userprofile/userprofile.component';
import { ChangepasswordComponent }  from './user/changepassword/changepassword.component';
import { StaticpageComponent} from './staticpage/staticpage.component';
import { StaticpageaddComponent} from './staticpage/staticpageadd/staticpageadd.component';
import { StaticpageeditComponent} from './staticpage/staticpageedit/staticpageedit.component';
import { StaticpageviewComponent} from './staticpage/staticpageview/staticpageview.component';

import { StageComponent} from './stage/intro/stage.component';
import { StageaddComponent} from './stage/intro/stageadd/stageadd.component';
import { StageeditComponent} from './stage/intro/stageedit/stageedit.component';
import { StageviewComponent} from './stage/intro/stageview/stageview.component';

import { ResearchComponent } from './stage/research/research.component';
import { ResearchaddComponent } from './stage/research/researchadd/researchadd.component';
import { ResearcheditComponent } from './stage/research/researchedit/researchedit.component';
import { ResearchviewComponent } from './stage/research/researchview/researchview.component';

import { GameComponent } from './game/game.component';
import { AddgameComponent } from './game/addgame/addgame.component';
import { EditgameComponent } from './game/editgame/editgame.component';

import { LogoComponent} from './logo/logo.component';
import { EditlogoComponent} from './logo/editlogo/editlogo.component';
import { AddlogoComponent} from './logo/addlogo/addlogo.component';
import { SettingsComponent} from './settings/settings.component';
import { GamevideoComponent} from './gamevideo/gamevideo.component';
import { AddgamevideoComponent} from './gamevideo/addgamevideo/addgamevideo.component';
import { EditgamevideoComponent} from './gamevideo/editgamevideo/editgamevideo.component';
import { ContentmanagerComponent} from './contentmanager/contentmanager.component';
import { AddcontentmanagerComponent} from './contentmanager/addcontentmanager/addcontentmanager.component';
import { EditcontentmanagerComponent} from './contentmanager/editcontentmanager/editcontentmanager.component';

import { EmailTemplateComponent} from './emailtemplate/emailtemplate.component';
import { EmailTemplateaddComponent} from './emailtemplate/emailtemplateadd/emailtemplateadd.component';
import { EmailTemplateeditComponent} from './emailtemplate/emailtemplateedit/emailtemplateedit.component';
import { EmailTemplateviewComponent} from './emailtemplate/emailtemplateview/emailtemplateview.component';

import { ContractorComponent } from './contractor/contractor.component';
import { AddcontractorComponent } from './contractor/addcontractor/addcontractor.component';
import { EditcontractorComponent } from './contractor/editcontractor/editcontractor.component';

import { BuildingComponent} from './building/building.component';
import { AddbuildingComponent } from './building/addbuilding/addbuilding.component';
import { EditbuildingComponent } from './building/editbuilding/editbuilding.component';

import { InspectorComponent } from './inspector/inspector.component';
import { AddinspectorComponent } from './inspector/addinspector/addinspector.component';
import { EditinspectorComponent } from './inspector/editinspector/editinspector.component';

import { QuestionComponent}  from './question/question.component';
import { AddquestionComponent} from './question/addquestion/addquestion.component';
import { EditquestionComponent} from './question/editquestion/editquestion.component';
import { AddanswerComponent } from './question/addanswer/addanswer.component';
import { ViewquestionComponent } from './question/viewquestion/viewquestion.component';

import { PlacementsComponent} from './placements/placements.component';
import { EditplacementComponent} from './placements/editplacement/editplacement.component';
import { AddplacementComponent} from './placements/addplacement/addplacement.component';
import { ViewplacementComponent}  from './placements/viewplacement/viewplacement.component';

import { SponsersComponent } from './sponsers/sponsers.component';
import { EditsponsersComponent } from './sponsers/editsponsers/editsponsers.component';
import { AddsponsersComponent } from './sponsers/addsponsers/addsponsers.component';

import {PlacementcategoryComponent} from './placementcategory/placementcategory.component';
import {AddplacementcategoryComponent} from './placementcategory/addplacementcategory/addplacementcategory.component';
import {EditplacementcategoryComponent} from './placementcategory/editplacementcategory/editplacementcategory.component';
/*
* services  
*/

import { AuthGuardService } from './services/auth-guard.service';
import { from } from 'rxjs';

const routes: Routes = [
  {
    path: 'users',canActivate: [AuthGuardService],
      children: [
        { path: '', component: UserindexComponent },
        { path: ':role_id', component: UserindexComponent},
        { path: 'add/:role_id', component: UseraddComponent },
        { path: 'edit/:user_id', component: UsereditComponent },
        { path: 'view/:user_id', component: UserviewComponent },
    ]
  },
  {
    path: 'placement-category',canActivate: [AuthGuardService],
      children: [
        { path: '', component: PlacementcategoryComponent },
        { path: 'add', component: AddplacementcategoryComponent },
        { path: 'edit/:id', component: EditplacementcategoryComponent },
    ]
  },
  {
    path: 'cms-manager',canActivate: [AuthGuardService],
      children: [
        { path: '', component: StaticpageComponent },
        { path: 'add', component: StaticpageaddComponent },
        { path: 'edit/:id', component: StaticpageeditComponent },
        { path: 'view/:id', component: StaticpageviewComponent },
    ]
  },
  {
    path: 'stage/stage-intro',canActivate: [AuthGuardService],
      children: [
        { path: '', component: StageComponent },
        { path: 'add', component: StageaddComponent },
        { path: 'edit/:id', component: StageeditComponent },
        { path: 'view/:id', component: StageviewComponent },
    ]
  },
  {
    path: 'stage/research-center',canActivate: [AuthGuardService],
      children: [
        { path: '', component: ResearchComponent },
        { path: 'add', component: ResearchaddComponent },
        { path: 'edit/:id', component: ResearcheditComponent },
        { path: 'view/:id', component: ResearchviewComponent },
    ]
  },
  {
    path: 'logo-manager',canActivate: [AuthGuardService],
      children: [
        { path: '', component: LogoComponent },
        { path: 'add', component: AddlogoComponent },
        { path: 'edit/:id', component: EditlogoComponent }
    ]
  },
  {
    path: 'sponsers',canActivate: [AuthGuardService],
      children: [
        { path: '', component: SponsersComponent },
        { path: 'add', component: AddsponsersComponent },
        { path: 'edit/:id', component: EditsponsersComponent }
    ]
  },
  {
    path: 'settings',canActivate: [AuthGuardService],
      children: [
        { path: 'game-settings', component: SettingsComponent }
    ]
  },
  {
    path: 'game-videos',canActivate: [AuthGuardService],
      children: [
        { path: '', component: GamevideoComponent },
        { path: 'add', component: AddgamevideoComponent},
        { path: 'edit/:id', component: EditgamevideoComponent}
    ]
  },
  {
    path: 'game',canActivate: [AuthGuardService],
      children: [
        { path: '', component: GameComponent },
        { path: 'add', component: AddgameComponent},
        { path: 'edit/:id', component: EditgameComponent}
    ]
  },
  {
    path: 'email-manager',canActivate: [AuthGuardService],
      children: [
        { path: '', component: EmailTemplateComponent },
        { path: 'add', component: EmailTemplateaddComponent },
        { path: 'edit/:id', component: EmailTemplateeditComponent },
        { path: 'view/:id', component: EmailTemplateviewComponent },
    ]
  },
  {
    path: 'content-manager',canActivate: [AuthGuardService],
      children: [
        { path: '', component: ContentmanagerComponent },
        { path: 'add', component: AddcontentmanagerComponent },
        { path: 'edit/:id', component: EditcontentmanagerComponent },
    ]
  },
  {
    path: 'contractor-manager',canActivate: [AuthGuardService],
      children: [
        { path: '', component: ContractorComponent },
        { path: 'add', component: AddcontractorComponent },
        { path: 'edit/:id', component: EditcontractorComponent },
    ]
  },
  {
    path: 'building-manager',canActivate: [AuthGuardService],
      children: [
        { path: '', component: BuildingComponent },
        { path: 'add', component: AddbuildingComponent },
        { path: 'edit/:id', component: EditbuildingComponent },
    ]
  },
  {
    path: 'inspector-messenger-manager',canActivate: [AuthGuardService],
      children: [
        { path: '', component: InspectorComponent },
        { path: 'add', component: AddinspectorComponent },
        { path: 'edit/:id', component: EditinspectorComponent },
    ]
  },
  {
    path: 'question-manager',canActivate: [AuthGuardService],
      children: [
        { path: '', component: QuestionComponent },
        { path: 'add', component: AddquestionComponent },
        { path: 'edit/:id', component: EditquestionComponent },
        { path: 'view/:id', component: ViewquestionComponent },
        { path: 'add-answer/:id', component: AddanswerComponent },
    ]
  },
  {
    path: 'placements-manager',canActivate: [AuthGuardService],
      children: [
        { path: '', component: PlacementsComponent },
        { path: 'add', component: AddplacementComponent },
        { path: 'edit/:id', component: EditplacementComponent },
        { path: 'view/:id', component: ViewplacementComponent },
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'myprofile', component: UserprofileComponent,canActivate: [AuthGuardService]},
  { path: 'changepassword', component: ChangepasswordComponent},
  { path: 'logout', component: LogoutComponent},
  { path: 'logo-manager', component: LogoComponent},
  
  { path: '**', redirectTo: '/login', pathMatch: 'full' }
]
@NgModule({
  imports: [
    CommonModule,RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
