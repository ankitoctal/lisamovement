import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { Title}  from '@angular/platform-browser';
import { EmailTemplateService } from '../../services/email-template.service';
import { environment } from '../../../environments/environment';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-emailtemplateedit',
  templateUrl: './emailtemplateedit.component.html',
  styleUrls: []
})

export class EmailTemplateeditComponent implements OnInit {
  editor = ClassicEditor;
  ediTemplateForm:FormGroup;   
  id: any = {};
  data :any;

  constructor(
    private emailTemplateService: EmailTemplateService , 
    private router: Router, 
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private _flashMessagesService: FlashMessagesService) {
  }

  ngOnInit() {

    this.titleService.setTitle(environment.siteName + ' - Edit Template');

    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    this.ediTemplateForm = new FormGroup({
        '_id': new FormControl(''),
        'title': new FormControl('', [ Validators.required]),
        'subject': new FormControl('',[ Validators.required, Validators.minLength(4) ]),
        'body': new FormControl('', [ Validators.required])
      });
    
    this.emailTemplateService.view(this.id).subscribe((response:any) => {
      this.ediTemplateForm.setValue({
          '_id':response.data._id,
          'title':response.data.title,
          'subject':response.data.subject,
          'body':response.data.body
      });
    });
  }

  public submitForm(){
   
      this.emailTemplateService.edit(this.ediTemplateForm.value).subscribe(response => {       		
        this._flashMessagesService.show('Email Template Updated Successfully', { cssClass: 'alert-success', timeout: 5000 });
      });

    this.router.navigate(['/email-manager']);	
  }

}
