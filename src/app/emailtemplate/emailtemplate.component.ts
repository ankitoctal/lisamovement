import { Component,ViewChild, OnInit} from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { EmailTemplateService } from '../services/email-template.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-emailtemplate',
  templateUrl: './emailtemplate.component.html',
  styleUrls: []
})

export class EmailTemplateComponent implements OnInit {

  @ViewChild("myckeditor") ckeditor: any;
  id: any = {}; 
  public details : any = {};

  constructor(
    private emailTemplateService: EmailTemplateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _flashMessagesService: FlashMessagesService) { 
  }

  @ViewChild('dataTable') table;
  tableWidget: any;
  dataTable: any;

  ngAfterViewInit() {
    this.initDatatable()
  }
  
  private initDatatable(): void {
    let exampleId: any = $('#example');
    this.tableWidget = exampleId.DataTable({
      select: true,
    });
  }

  delete(id){
    this.emailTemplateService.delete(id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show('Static Page has been deleted successfully', { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      }                      
    });
  }

  ngOnInit() {
    
    this.activatedRoute.params.subscribe(routeParams => {
      this.emailTemplateService.getTemplateDetails().subscribe((response: any) => {
          this.details = response.data;
          console.log(response.data);
      });
    });

    this.dataTable = $(this.table.nativeElement);
  }

}
