import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { EmailTemplateService } from '../../services/email-template.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
import { Title}  from '@angular/platform-browser';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-emailtemplateadd',  
  templateUrl: './emailtemplateadd.component.html',
  styleUrls: []
})

export class EmailTemplateaddComponent implements OnInit {
  templateForm: FormGroup; 
  user_role :any = {};
  error ='';
  editor = ClassicEditor;
  data:any;

  constructor(
    private emailTemplateService: EmailTemplateService,
    private _flashMessagesService: FlashMessagesService,
    private activatedRoute:ActivatedRoute,
    private router: Router,
    private titleService: Title) { }

  ngOnInit() {

    this.titleService.setTitle(environment.siteName + ' - Email Template');

      this.templateForm = new FormGroup({
        '_id': new FormControl(''),
        'title': new FormControl('', [ Validators.required]),
        'subject': new FormControl('',[ Validators.required, Validators.minLength(4) ]),
        'body': new FormControl('', [ Validators.required])
      });
  }


  public submitForm(){ 

    this.emailTemplateService.add(this.templateForm.value).subscribe(response => {       		
        if(response["status"] == 'true')
        { 
          this._flashMessagesService.show('Email Template has been successfully added.', { cssClass: 'alert-success', timeout: 5000 });
          this.router.navigate(['/email-manager']);

        }else {
          this.error = response["msg"];
        }	
    });
     
  }

}

