import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router,ActivatedRoute } from "@angular/router";
import { EmailTemplateService } from '../../services/email-template.service';

@Component({
  selector: 'app-emailtemplateview',
  templateUrl: './emailtemplateview.component.html',
  styleUrls: []
})

export class EmailTemplateviewComponent implements OnInit {

  constructor(
    private emailTemplateService: EmailTemplateService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute) { }

  id: any = {}; 
  data: any = {}; 

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');	
    this.emailTemplateService.view(this.id).subscribe((response:any) => {
      console.log(response.data)
      this.data = response.data;	  		  
    });
  }

}
