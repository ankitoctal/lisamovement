import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacementcategoryComponent } from './placementcategory.component';

describe('PlacementcategoryComponent', () => {
  let component: PlacementcategoryComponent;
  let fixture: ComponentFixture<PlacementcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacementcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacementcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
