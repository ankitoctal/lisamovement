import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { PlacementCategoryService } from '../../services/placement-category.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-addplacementcategory',
  templateUrl: './addplacementcategory.component.html',
  styleUrls: ['./addplacementcategory.component.css']
})
export class AddplacementcategoryComponent implements OnInit {

  dataForm:FormGroup;
  error ='';

  constructor(private _flashMessagesService: FlashMessagesService,private placementCategoryService: PlacementCategoryService,private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.dataForm = new FormGroup({
        'name': new FormControl('',[
          Validators.required      
        ]), 
 		
    });
  }
  
  public submitForm(){ 	
    var str = this.placementCategoryService.add(this.dataForm.value).subscribe(response => {       		
         if(response["status"] == 'true')
         { 
          this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
          this.router.navigate(['/placement-category']);	
          
      }
      else
      {
        this.error = response["msg"];
      }	
    });
     
  }

}
