import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddplacementcategoryComponent } from './addplacementcategory.component';

describe('AddplacementcategoryComponent', () => {
  let component: AddplacementcategoryComponent;
  let fixture: ComponentFixture<AddplacementcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddplacementcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddplacementcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
