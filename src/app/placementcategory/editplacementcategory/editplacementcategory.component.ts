import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { PlacementCategoryService } from '../../services/placement-category.service';

@Component({
  selector: 'app-editplacementcategory',
  templateUrl: './editplacementcategory.component.html',
  styleUrls: ['./editplacementcategory.component.css']
})
export class EditplacementcategoryComponent implements OnInit {

  constructor(private placementCategoryService: PlacementCategoryService , private router: Router, private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService) { }
  userForm: any = {};  	
  id: any = {};
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');	   		
    this.placementCategoryService.view(this.id).subscribe((response:any) => { 
      this.userForm = response.data;
      console.log(this.userForm);	  
    });
  }
  public submitForm(){
      this.placementCategoryService.edit(this.userForm).subscribe(response => {       		
      this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
    });
    this.router.navigate(['/placement-category']);	
  }

}
