import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditplacementcategoryComponent } from './editplacementcategory.component';

describe('EditplacementcategoryComponent', () => {
  let component: EditplacementcategoryComponent;
  let fixture: ComponentFixture<EditplacementcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditplacementcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditplacementcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
