import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { Router,ActivatedRoute } from "@angular/router"
import {DatepickerOptions} from '../../ng-datepicker/component/ng-datepicker.component';
import * as enLocale from 'date-fns/locale/en';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {
  date: Date;
  options: DatepickerOptions = {
    locale: enLocale
  };
  endDate : any;
  startDate: any;
  constructor(private _flashMessagesService: FlashMessagesService,private commonService: CommonService,private router: Router, private activatedRoute: ActivatedRoute) {
    //this.startDate = new Date(2005, 1, 4);
   }
  dataForm: any = {}; 
  ngOnInit() { 
    
    this.commonService.getSettingDetails().subscribe((response:any) => { 
      this.dataForm = response.data;
      this.startDate = new Date(this.dataForm.game_start_date);
      this.endDate = new Date(this.dataForm.game_end_date);
    });
  }
  public submitForm(){
    this.commonService.saveGameSetting(this.dataForm).subscribe(response => {   		
    this._flashMessagesService.show("u[dated successfully", { cssClass: 'alert-success', timeout: 5000 });
     this.router.navigate(['/settings/game-settings']);
   });
  }
  set humanStartDate(e){
    e = e.split('-');
    let d = new Date(Date.UTC(e[0], e[1]-1, e[2]));
    this.startDate.setFullYear(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
  }

  get humanStartDate(){
    return this.startDate.toISOString().substring(0, 10);
  }
  set humanEndDate(e){
    e = e.split('-');
    let d = new Date(Date.UTC(e[0], e[1]-1, e[2]));
    this.endDate.setFullYear(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
  }

  get humanEndDate(){
    return this.endDate.toISOString().substring(0, 10);
  }
}
