import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute,UrlTree,UrlSegmentGroup,UrlSegment,PRIMARY_OUTLET} from "@angular/router";
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  public url:any={};
  public isLoginRoute:any={};
  constructor(private activatedRoute:ActivatedRoute,private router:Router) { 
    const tree: UrlTree = router.parseUrl(location.pathname);
    const g: UrlSegmentGroup = tree.root.children[PRIMARY_OUTLET];
    const s: UrlSegment[] = g.segments;
    s[0].path; // returns 'team'
    s[0].parameters; // returns {id: 33}
    this.isLoginRoute = s[0].path;
  }
  ngOnInit() {
    this.router.events.subscribe((e) => {
      this.url = this.activatedRoute.snapshot.url;
     });
  }

}
