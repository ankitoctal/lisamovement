import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-addgamevideo',
  templateUrl: './addgamevideo.component.html',
  styleUrls: ['./addgamevideo.component.css']
})

export class AddgamevideoComponent implements OnInit {

  dataForm:FormGroup;
  error ='';

  constructor(private _flashMessagesService: FlashMessagesService,private commonService: CommonService,private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.dataForm = new FormGroup({
        'title': new FormControl(''),
        'video': new FormControl('',[
          Validators.required,Validators.minLength(4), ]),        
        'short_description': new FormControl(''),
        'category': new FormControl('',[
          Validators.required      
        ]), 
        'confirm_password': new FormControl('',[
          Validators.minLength(1),        		        
        ])  		
    },ValidateVideoUrl);
  }
  
  public submitForm(){ 	
    var str = this.commonService.addGameVideo(this.dataForm.value).subscribe(response => {       		
         if(response["status"] == 'true')
         { 
          this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
          this.router.navigate(['/game-videos']);	
          
      }
      else
      {
        this.error = response["msg"];
      }	
    });
     
  }

}

export function ValidateVideoUrl(findForm: FormControl) {
  var url = findForm["controls"].video.value;
  if (url != undefined || url != '') {        
      var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
      var match = url.match(regExp);
      if (match && match[2].length == 11) {
          // Do anything for being valid
          return null;
          // if need to change the url to embed url then use below line            
         // $('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
      } else {
          return { validConfirmPassword: true };
          // Do anything for not being valid
      }
  }
  return { validVideoURL: true };
  
}
