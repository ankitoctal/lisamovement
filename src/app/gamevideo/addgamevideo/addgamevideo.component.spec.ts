import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddgamevideoComponent } from './addgamevideo.component';

describe('AddgamevideoComponent', () => {
  let component: AddgamevideoComponent;
  let fixture: ComponentFixture<AddgamevideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddgamevideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddgamevideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
