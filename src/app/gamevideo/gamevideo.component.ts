import { Component, OnInit,ViewChild } from '@angular/core';
import { CommonService } from '../services/common.service';
import { Router,ActivatedRoute } from "@angular/router"
import { FlashMessagesService } from 'angular2-flash-messages';
import * as $ from 'jquery';
import 'datatables.net';
@Component({
  selector: 'app-gamevideo',
  templateUrl: './gamevideo.component.html',
  styleUrls: ['./gamevideo.component.css']
})
export class GamevideoComponent implements OnInit {
  
  user_id: any = {}; 
  public details: any = [];
  public data : any = [];
  categories = []; 
  constructor(private commonService: CommonService,private router: Router,private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService) {
    this.categories = ["Strategies to Win", "About the Building", "Inspectors",'Messengers']; 
  }

  @ViewChild("dataTable") table: any;
  tableWidget: any;
  dataTable: any;

  ngAfterViewInit() {
    this.initDatatable();
    console.log(this.table.nativeElement);
  }

  private initDatatable(): void {
    let exampleId: any = $('#example');
    this.tableWidget = exampleId.DataTable({
      select: true,
    });
  }
  delete(user_id){
    this.commonService.deleteGameVideo(user_id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      }                      
    });
  }
  updateStatus(id,status){
    this.commonService.updateStatusGameVideo(id,status).subscribe((response: any) => {
      if(response["status"] == 'true'){
        location.reload(true);
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
      }                      
    });
  }
  ngOnInit() { 
    this.activatedRoute.params.subscribe(routeParams => {
      this.commonService.getGameVideosDetails().subscribe((response: any) => {
        this.details = response.data;                    
      });
    });
    this.dataTable = $(this.table.nativeElement);
  }
}
