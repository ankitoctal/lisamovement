import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-editgamevideo',
  templateUrl: './editgamevideo.component.html',
  styleUrls: ['./editgamevideo.component.css']
})
export class EditgamevideoComponent implements OnInit {

  constructor(private commonService: CommonService , private router: Router, private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService) { }
  userForm: any = {};  	
  id: any = {};
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');	   		
    this.commonService.viewGameVideo(this.id).subscribe((response:any) => { 
      this.userForm = response.data;
      console.log(this.userForm);	  
    });
  }
  public submitForm(){
    this.commonService.editGameVideo(this.userForm).subscribe(response => {       		
    this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
  });
  this.router.navigate(['/game-videos']);	
}

}
