import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContractorService } from '../../services/contractor.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
@Component({
  selector: 'app-addcontractor',
  templateUrl: './addcontractor.component.html',
  styleUrls: ['./addcontractor.component.css']
})
export class AddcontractorComponent implements OnInit {
  dataForm: FormGroup; 
  error ='';
  categoryArray = [];
  constructor(private contractorService: ContractorService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) {  }

  ngOnInit() {
    this.categoryArray = [
      {name: 'Concrete', abbrev: '1'},
      {name: 'Electrical', abbrev: '2'},
      {name: 'Mechanical', abbrev: '3'},
      {name: 'Plumbing', abbrev: '4'},
      {name: 'Drywall', abbrev: '5'},
      {name: 'Curtain Wall', abbrev: '6'},
      {name: 'Finish', abbrev: '7'},
    ];
    this.dataForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required        
      ]),
      'speed': new FormControl('', [
        Validators.required        
      ]),
      'cost': new FormControl('', [
        Validators.required        
      ]),
      'quality': new FormControl('', [
        Validators.required        
      ]),
      'safty': new FormControl('', [
        Validators.required        
      ]),
      'category': new FormControl('', [
        Validators.required        
      ])
  });
  }
  get f(){
    return this.dataForm.controls;
  }

  public submitForm(){ 	
    var str = this.contractorService.add(this.dataForm.value).subscribe(response => {       		
      if(response["status"] == 'true')
      { 
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        this.router.navigate(['/contractor-manager']);	
      }
      else
      {
        this.error = response["msg"];
      }	
    });
     
  }

}
