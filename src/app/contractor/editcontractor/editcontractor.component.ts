import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { ContractorService } from '../../services/contractor.service';

@Component({
  selector: 'app-editcontractor',
  templateUrl: './editcontractor.component.html',
  styleUrls: ['./editcontractor.component.css']
})
export class EditcontractorComponent implements OnInit {
  dataForm: any = [];  	
  id: string; 
  categoryArray = [];
  constructor(private contractorService: ContractorService,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.categoryArray = [
      {name: 'Concrete', abbrev: '1'},
      {name: 'Electrical', abbrev: '2'},
      {name: 'Mechanical', abbrev: '3'},
      {name: 'Plumbing', abbrev: '4'},
      {name: 'Drywall', abbrev: '5'},
      {name: 'Curtain Wall', abbrev: '6'},
      {name: 'Finish', abbrev: '7'},
    ];
    this.id = this.activatedRoute.snapshot.paramMap.get('id');	
    this.activatedRoute.params.subscribe(routeParams => {
      this.contractorService.view(routeParams.id).subscribe((response: any) => {
        this.dataForm = response.data;             
      });
    });
  }
  public submitForm(){
      this.contractorService.edit(this.dataForm).subscribe(response => {       		
      this._flashMessagesService.show(response['msg'], { cssClass: 'alert-success', timeout: 5000 });
    });
    this.router.navigate(['/contractor-manager']);	
  }

}
