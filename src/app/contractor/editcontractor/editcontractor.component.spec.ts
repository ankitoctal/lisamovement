import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditcontractorComponent } from './editcontractor.component';

describe('EditcontractorComponent', () => {
  let component: EditcontractorComponent;
  let fixture: ComponentFixture<EditcontractorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditcontractorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditcontractorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
