import { Component, ViewChild, OnInit } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { ContractorService } from '../services/contractor.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-contractor',
  templateUrl: './contractor.component.html',
  styleUrls: ['./contractor.component.css']
})
export class ContractorComponent implements OnInit {

  public details : any =[];
  categoryArray = [];
  constructor(private contractorService: ContractorService,private router: Router,private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService) {
    
   }
  @ViewChild("dataTable") table: any;
  tableWidget: any;
  dataTable: any;

  ngAfterViewInit() {
    this.initDatatable();
  }

  private initDatatable(): void {
    let exampleId: any = $('#example');
    this.tableWidget = exampleId.DataTable({
      select: true,
    });
  }
  delete(id){
    this.contractorService.delete(id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      }
    });
  }
  ngOnInit() {
    this.categoryArray = [
      {name: 'Concrete', abbrev: '1'},
      {name: 'Electrical', abbrev: '2'},
      {name: 'Mechanical', abbrev: '3'},
      {name: 'Plumbing', abbrev: '4'},
      {name: 'Drywall', abbrev: '5'},
      {name: 'Curtain Wall', abbrev: '6'},
      {name: 'Finish', abbrev: '7'}
    ];

    this.activatedRoute.params.subscribe(routeParams => {
      this.contractorService.getDetails().subscribe((response: any) => {
        this.details = response.data;    
      });
    });

    this.dataTable = $(this.table.nativeElement);
  }
  updateStatus(id,status){
    this.contractorService.updateStatus(id,status).subscribe((response: any) => {
      if(response["status"] == 'true'){
        location.reload(true);
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
      }                      
    });
  }

}
