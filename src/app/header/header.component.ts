import { Component, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import * as $ from 'jquery';
// import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-header',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './header.component.html'
})
export class HeaderComponent{
    public pageName = 'Home';
    public currentUser :any = {};
    constructor(public router: Router) {
    }
    
    redirectToHome() {
        this.router.navigateByUrl('dashboard/home');
    }
    redirectToAbout() {
        this.router.navigateByUrl('dashboard/about');
    }
    logMeOut(){
        localStorage.clear();
        window.location.reload(); 
        //this.router.navigateByUrl('login');
    }
    redirectToUsermanagement(){
        this.router.navigateByUrl('users');
    }
    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('token'));
    }
}
