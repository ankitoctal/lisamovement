import { Component,ViewChild, OnInit } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { ContentService } from '../services/content.service';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-contentmanager',
  templateUrl: './contentmanager.component.html',
  styleUrls: ['./contentmanager.component.css']
})
export class ContentmanagerComponent implements OnInit {
  public details : any = {};
  constructor(private contentService: ContentService,private router: Router,private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService) { }
  @ViewChild("dataTable") table: any;
  tableWidget: any;
  dataTable: any;

  ngAfterViewInit() {
    this.initDatatable();
  }

  private initDatatable(): void {
    let exampleId: any = $('#example');
    this.tableWidget = exampleId.DataTable({
      select: true,
    });
  }
  delete(id){
    this.contentService.delete(id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      }
    });
  }
  ngOnInit() {
    this.activatedRoute.params.subscribe(routeParams => {
      this.contentService.getDetails().subscribe((response: any) => {
        this.details = response.data;    
      });
    });
    this.dataTable = $(this.table.nativeElement);
  }

}
