import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditcontentmanagerComponent } from './editcontentmanager.component';

describe('EditcontentmanagerComponent', () => {
  let component: EditcontentmanagerComponent;
  let fixture: ComponentFixture<EditcontentmanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditcontentmanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditcontentmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
