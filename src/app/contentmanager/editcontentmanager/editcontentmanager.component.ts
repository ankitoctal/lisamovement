import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { ContentService } from '../../services/content.service';
@Component({
  selector: 'app-editcontentmanager',
  templateUrl: './editcontentmanager.component.html',
  styleUrls: ['./editcontentmanager.component.css']
})
export class EditcontentmanagerComponent implements OnInit {
  dataForm: any = [];  	
  id: string; 
  endDate : any;
  startDate: any;
  constructor(private contentService: ContentService,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');	 

    this.activatedRoute.params.subscribe(routeParams => {
      this.contentService.view(routeParams.id).subscribe((response: any) => {
        this.dataForm = response.data;
        this.startDate = this.dataForm.game_start_date;
        this.endDate = this.dataForm.game_end_date;                  
      });
    });
  }
  public submitForm(){
      this.contentService.edit(this.dataForm).subscribe(response => {       
        var slug = this.id;
        //this.router.navigate(['/content-manager/edit/',slug]);			
    });
    
  }
  set humanStartDate(e){
    e = e.split('-');
    let d = new Date(Date.UTC(e[0], e[1]-1, e[2]));
    this.startDate.setFullYear(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
  }

  get humanStartDate(){
    return this.startDate.toISOString().substring(0, 10);
  }
  set humanEndDate(e){
    e = e.split('-');
    let d = new Date(Date.UTC(e[0], e[1]-1, e[2]));
    this.endDate.setFullYear(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
  }

  get humanEndDate(){
    return this.endDate.toISOString().substring(0, 10);
  }
}
