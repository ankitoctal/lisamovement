import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContentService } from '../../services/content.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
@Component({
  selector: 'app-addcontentmanager',
  templateUrl: './addcontentmanager.component.html',
  styleUrls: ['./addcontentmanager.component.css']
})
export class AddcontentmanagerComponent implements OnInit {
  dataForm: FormGroup; 
  error ='';
  endDate : any;
  startDate: any;
  constructor(private contentService: ContentService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) { 
    this.startDate = new Date();
    this.endDate = new Date();
  }

  ngOnInit() {
    this.dataForm = new FormGroup({
      'page_name': new FormControl('', [
        Validators.required        
      ]),
      'start_date': new FormControl('', [
        Validators.required        
      ]),
      'end_date': new FormControl('', [
        Validators.required,       
      ]),
      'main_page_title': new FormControl(''),
      'main_page_subtitle': new FormControl(''),
  });
  }
  get f(){
    return this.dataForm.controls;
  }

  public submitForm(){ 	
    var str = this.contentService.add(this.dataForm.value).subscribe(response => {       		
      if(response["status"] == 'true')
      { 
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        this.router.navigate(['/content-manager']);	
      }
      else
      {
        this.error = response["msg"];
      }	
    });
     
  }

  set humanStartDate(e){
    e = e.split('-');
    let d = new Date(Date.UTC(e[0], e[1]-1, e[2]));
    this.startDate.setFullYear(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
  }

  get humanStartDate(){
    return this.startDate.toISOString().substring(0, 10);
  }
  set humanEndDate(e){
    e = e.split('-');
    let d = new Date(Date.UTC(e[0], e[1]-1, e[2]));
    this.endDate.setFullYear(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
  }

  get humanEndDate(){
    return this.endDate.toISOString().substring(0, 10);
  }

}
