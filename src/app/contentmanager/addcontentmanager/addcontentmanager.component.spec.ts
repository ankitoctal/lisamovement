import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcontentmanagerComponent } from './addcontentmanager.component';

describe('AddcontentmanagerComponent', () => {
  let component: AddcontentmanagerComponent;
  let fixture: ComponentFixture<AddcontentmanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcontentmanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcontentmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
