import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { UserService } from './services/user.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public currentUser = "";
  title = 'Lisa App';
  isLoginRoute: string;
  constructor (private router: Router,private user:UserService,) {   
    this.router.events.subscribe((e) => {
     this.isLoginRoute = this.router.url;
    });
  }
  
  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('token'));
  }

}
