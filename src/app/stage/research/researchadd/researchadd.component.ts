import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { StageService } from '../../../services/stage.service';
import { GameService } from '../../../services/game.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
import { Title}  from '@angular/platform-browser';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-researchadd',  
  templateUrl: './researchadd.component.html',
  styleUrls: []
})

export class ResearchaddComponent implements OnInit {
  userForm: FormGroup; 
  user_role :any = {};
  gameDetail :any = [];
  error ='';
  editor = ClassicEditor;
  data:any;

  constructor(
    private stageService: StageService,
    private gameService: GameService,
    private http:HttpClient,
    private _flashMessagesService: FlashMessagesService,
    private activatedRoute:ActivatedRoute,
    private router: Router,
    private titleService: Title) { }

  ngOnInit() {

    this.titleService.setTitle(environment.siteName + ' - Stage Intro');

    this.gameService.getDetails().subscribe((response: any) => {
        this.gameDetail = response.data;
    });

    this.userForm = new FormGroup({
        'title': new FormControl('', [ Validators.required]),
        'gameId': new FormControl('', [ Validators.required]),
        'stage': new FormControl('', [ Validators.required]),
        'content': new FormControl('', [ Validators.required])
      });
  }


  public submitForm(){ 

    this.stageService.addResearch(this.userForm.value).subscribe(response => {       		
        if(response["status"] == 'true')
        { 
          this._flashMessagesService.show('Stage Content has been successfully added.', { cssClass: 'alert-success', timeout: 5000 });
          this.router.navigate(['/stage/research-center']);

        }else {
          this.error = response["msg"];
        }	
    });
     
  }

}
