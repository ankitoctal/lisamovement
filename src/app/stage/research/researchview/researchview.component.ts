import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router,ActivatedRoute } from "@angular/router";
import { StageService } from '../../../services/stage.service';

@Component({
  selector: 'app-researchview',
  templateUrl: './researchview.component.html',
  styleUrls: []
})

export class ResearchviewComponent implements OnInit {

  constructor(
    private stageService: StageService ,
    private userService: UserService,
    private activatedRoute: ActivatedRoute) { }

  id: any = {}; 
  data: any = {}; 

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');	
    this.stageService.viewResearchDetail(this.id).subscribe((response:any) => { 
      this.data = response.data;	  		  
    });
  }

}
