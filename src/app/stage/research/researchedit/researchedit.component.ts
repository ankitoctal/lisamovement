import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { Title}  from '@angular/platform-browser';
import { StageService } from '../../../services/stage.service';
import { environment } from '../../../../environments/environment';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-researchedit',
  templateUrl: './researchedit.component.html',
  styleUrls: []
})

export class ResearcheditComponent implements OnInit {
  editor = ClassicEditor;
  editUserForm:FormGroup;   
  id: any = {};
  data:any;

  constructor(
    private stageService: StageService ,
    private router: Router, 
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private _flashMessagesService: FlashMessagesService) {
  }

  ngOnInit() {

    this.titleService.setTitle(environment.siteName + ' - Edit');

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.editUserForm = new FormGroup({
        '_id': new FormControl(''),
        'title': new FormControl('', [ Validators.required]),
        'gameId': new FormControl('', [ Validators.required]),
        'stage': new FormControl('', [ Validators.required]),
        'content': new FormControl('', [ Validators.required])
      });
    
    this.stageService.viewResearchDetail(this.id).subscribe((response:any) => {
      this.editUserForm.setValue({
          '_id':response.data._id,
          'title':response.data.title,
          'gameId':response.data.gameId,
          'stage':response.data.stage,
          'content':response.data.content
      });
    });
  }

  public submitForm(){
   
      this.stageService.editResearchDetail(this.editUserForm.value).subscribe(response => {       		
        this._flashMessagesService.show('Stage Content Updated Successfully', { cssClass: 'alert-success', timeout: 5000 });
      });

    this.router.navigate(['/stage/research-center']);	
  }

}

