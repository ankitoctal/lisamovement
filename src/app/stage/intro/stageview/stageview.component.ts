import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router,ActivatedRoute } from "@angular/router";
import { StageService } from '../../../services/stage.service';

@Component({
  selector: 'app-stageview',
  templateUrl: './stageview.component.html',
  styleUrls: []
})

export class StageviewComponent implements OnInit {

  constructor(
    private stageIntroService: StageService ,
    private userService: UserService,
    private activatedRoute: ActivatedRoute) { }

  id: any = {}; 
  data: any = {}; 

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');	
    this.stageIntroService.viewStageDetail(this.id).subscribe((response:any) => { 
      this.data = response.data;	  		  
    });
  }

}
