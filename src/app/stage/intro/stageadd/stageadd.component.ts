import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { StageService } from '../../../services/stage.service';
import { GameService } from '../../../services/game.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router, ActivatedRoute} from "@angular/router";
import { Title }  from '@angular/platform-browser';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-stageadd',  
  templateUrl: './stageadd.component.html',
  styleUrls: []
})

export class StageaddComponent implements OnInit {
  userForm: FormGroup; 
  user_role :any = {};
  error ='';
  editor = ClassicEditor;
  gameDetail: any = [];
  data :any;

  constructor(
    private stageIntroService: StageService,
    private gameService: GameService,
    private http:HttpClient,
    private _flashMessagesService: FlashMessagesService,
    private activatedRoute:ActivatedRoute,
    private router: Router,
    private titleService: Title) { }

  ngOnInit() {

    this.gameService.getDetails().subscribe((response: any) => {
        this.gameDetail = response.data;
    });

    this.titleService.setTitle(environment.siteName + ' - Stage Intro');

    this.userForm = new FormGroup({
        'title': new FormControl('', [ Validators.required]),
        'saving_amount': new FormControl('', [ Validators.required]),
        'gameId': new FormControl('', [ Validators.required]),
        'video': new FormControl('',[ Validators.required, Validators.minLength(4) ]),
        'stage': new FormControl('', [ Validators.required]),
        'researchCenter': new FormControl('', [ Validators.required]),        
        'strategyDetail': new FormControl('', [ Validators.required ])
      },ValidateVideoUrl);
  }


  public submitForm(){ 

    this.stageIntroService.addStage(this.userForm.value).subscribe(response => {       		
        if(response["status"] == 'true')
        { 
          this._flashMessagesService.show('Stage Intro has been successfully added.', { cssClass: 'alert-success', timeout: 5000 });
          this.router.navigate(['/stage/stage-intro']);

        }else {
          this.error = response["msg"];
        }	
    });
     
  }

}

export function ValidateVideoUrl(findForm: FormControl) {
  var url = findForm["controls"].video.value;
  if (url != undefined || url != '') {        
      var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
      var match = url.match(regExp);
      if (match && match[2].length == 11) {
          // Do anything for being valid
          return null;
          // if need to change the url to embed url then use below line            
         // $('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
      } else {
          return { validConfirmPassword: true };
       
      }
  }
  return { validVideoURL: true };
  
}
