import { Component,ViewChild, OnInit,ChangeDetectorRef} from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { StageService } from '../../services/stage.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-stage-intro',
  templateUrl: './stage.component.html',
  styleUrls: []
})

export class StageComponent implements OnInit {
  @ViewChild("myckeditor") ckeditor: any;
  // CK editor configuration finsh here
  id: any = {}; 
  public details : any;
  dataTable: any;

  constructor(
    private stageIntroService: StageService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _flashMessagesService: FlashMessagesService,
    private chRef: ChangeDetectorRef) { 
  }

  ngOnInit() {
    
    this.activatedRoute.params.subscribe(routeParams => {
      this.stageIntroService.getStageDetails().subscribe((response: any) => {
        this.details = response.data;  
        this.chRef.detectChanges();
        const table: any = $('table');
        this.dataTable = table.DataTable();                  
      });
    });
  }

}
