import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { Title}  from '@angular/platform-browser';
import { StageService } from '../../../services/stage.service';
import { GameService } from '../../../services/game.service';
import { environment } from '../../../../environments/environment';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-stageedit',
  templateUrl: './stageedit.component.html',
  styleUrls: []
})

export class StageeditComponent implements OnInit {
  editor = ClassicEditor;
  editUserForm:FormGroup;
  gameDetail: any = {};
  id: any = {};
  data :any;

  constructor(
    private stageIntroService: StageService , 
    private gameService: GameService , 
    private router: Router, 
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private _flashMessagesService: FlashMessagesService) {
  }

  ngOnInit() {

    this.titleService.setTitle(environment.siteName + ' - Edit Intro');	
    this.id = this.activatedRoute.snapshot.paramMap.get('id');

   
    this.editUserForm = new FormGroup({
        '_id': new FormControl(''),
        'gameId': new FormControl(''),
        'title': new FormControl('', [ Validators.required]),
        'saving_amount': new FormControl('', [ Validators.required]),
        'video': new FormControl('',[ Validators.required, Validators.minLength(4) ]),
        'stage': new FormControl('', [ Validators.required]),
        'researchCenter': new FormControl('', [ Validators.required]),        
        'strategyDetail': new FormControl('', [ Validators.required ])
      },ValidateVideoUrl);
    
    this.stageIntroService.viewStageDetail(this.id).subscribe((response:any) => {
      this.editUserForm.setValue({
          '_id':response.data._id,
          'title':response.data.title,
          'saving_amount':response.data.saving_amount,
          'gameId':response.data.gameId,
          'video':response.data.video,
          'stage':response.data.stage,
          'researchCenter':response.data.researchCenter,
          'strategyDetail':response.data.strategyDetail
      });
    });
  }

  public submitForm(){
   
      this.stageIntroService.editStageDetail(this.editUserForm.value).subscribe(response => {       		
        this._flashMessagesService.show('Stage Intro Updated Successfully', { cssClass: 'alert-success', timeout: 5000 });
      });

    this.router.navigate(['/stage/stage-intro']);	
  }

}

export function ValidateVideoUrl(findForm: FormControl) {
  var url = findForm["controls"].video.value;
  if (url != undefined || url != '') {        
      var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
      var match = url.match(regExp);
      if (match && match[2].length == 11) {
          // Do anything for being valid
          return null;
          // if need to change the url to embed url then use below line            
         // $('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
      } else {
          return { validConfirmPassword: true };
       
      }
  }
  return { validVideoURL: true };
  
}
