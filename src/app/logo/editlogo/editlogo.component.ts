import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute } from "@angular/router"
import { CommonService } from '../../services/common.service';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-editlogo',
  templateUrl: './editlogo.component.html',
  styleUrls: ['./editlogo.component.css']
})
export class EditlogoComponent implements OnInit {
  fileToUpload :any;
  env = environment;
  constructor(private commonService: CommonService , private router: Router, private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService) { }
  userForm: any = {};  	
  id: any = {};
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');	   		
    this.commonService.view(this.id).subscribe((response:any) => { 
      this.userForm = response.data;
      console.log(this.userForm);	  
    });
  }
  onFileChange(event) { 
    if (event.target.files.length > 0) {
      this.fileToUpload  = event.target.files[0];
    }
  }
  public submitForm(){
      const formData = new FormData();
      formData.append('company', this.userForm.company);
      formData.append('link', this.userForm.link);
      formData.append('focus', this.userForm.focus);
      formData.append('employee', this.userForm.employee);
      formData.append('reach', this.userForm.reach);
      formData.append('_id', this.userForm._id);
      if(this.fileToUpload != undefined){
        formData.append('image', this.fileToUpload, this.fileToUpload.name);
      }
      //formData.append('description', this.userForm.description);
      this.commonService.edit(formData).subscribe(response => {       		
      this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
    });
    this.router.navigate(['/logo-manager']);	
  }
}
