import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditlogoComponent } from './editlogo.component';

describe('EditlogoComponent', () => {
  let component: EditlogoComponent;
  let fixture: ComponentFixture<EditlogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditlogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditlogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
