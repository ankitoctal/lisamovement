import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import { Router,ActivatedRoute } from "@angular/router";
import { CommonService } from '../services/common.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { environment } from '../../environments/environment';
@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css']
})
export class LogoComponent implements OnInit {

  id: any = {}; 
  status: any = {}; 
  public details : any;
  env = environment;
  dataTable: any;
  constructor(private commonService: CommonService,private router: Router,private activatedRoute: ActivatedRoute,private _flashMessagesService: FlashMessagesService,private chRef: ChangeDetectorRef) { 
  }

  delete(id){
    this.commonService.delete(id).subscribe((response: any) => {
      if(response["status"] == 'true'){
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        location.reload(true);
      }                      
    });
  }
  
  updateStatus(id,status){
    this.commonService.updateStatus(id,status).subscribe((response: any) => {
      if(response["status"] == 'true'){
        location.reload(true);
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
      }                      
    });
  }
  ngOnInit() {
    this.activatedRoute.params.subscribe(routeParams => {
      this.commonService.getDetails().subscribe((response: any) => {
        this.details = response.data;   
        this.chRef.detectChanges();
        const table: any = $('table');
        this.dataTable = table.DataTable();
      });
    });
  }

}
