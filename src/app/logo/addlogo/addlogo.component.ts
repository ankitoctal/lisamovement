import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router,ActivatedRoute} from "@angular/router";
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-addlogo',
  templateUrl: './addlogo.component.html',
  styleUrls: ['./addlogo.component.css']
})
export class AddlogoComponent implements OnInit {

  dataForm: FormGroup; 
  error ='';
  fileToUpload :any;
  sendFilevalue :any;
  image: File;
  resData: any;
  selectedFile = null;
  constructor(private commonService: CommonService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private activatedRoute:ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.dataForm = new FormGroup({
        'company': new FormControl('', [
          Validators.required        
        ]),
        file: new FormControl('', [Validators.required]),
        'link': new FormControl('', [
          Validators.required,       
        ]),
        'focus':new FormControl(),
        'employee':new FormControl(),
        'reach':new FormControl()  
    });
  }
  get f(){
    return this.dataForm.controls;
  }
  onFileChange(event) {
  
    if (event.target.files.length > 0) {
      this.fileToUpload  = event.target.files[0];
    }
  }
  public submitForm(){ 	
    const payload = new FormData();
    payload.append('company', this.dataForm.value.company);
    payload.append('link', this.dataForm.value.link);
    payload.append('focus', this.dataForm.value.focus);
    payload.append('employee', this.dataForm.value.employee);
    payload.append('reach', this.dataForm.value.reach);


    payload.append('image', this.fileToUpload, this.fileToUpload.name);
    var str = this.commonService.add(payload).subscribe(response => {       		
      if(response["status"] == 'true')
      { 
        this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
        this.router.navigate(['/logo-manager']);	
      }
      else
      {
        this.error = response["msg"];
      }	
    });
  }
}
