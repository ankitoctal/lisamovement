// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  siteName :'LisaMovement',
  apiUrl : 'http://localhost:4006/adminapi',
  serverUrl : 'http://localhost:4006',
  instagramUrl : 'https://www.instagram.com/',
  facebookUrl : 'https://www.facebook.com/',
  linkedinUrl : 'https://www.linkedin.com/',
  twitterUrl : 'https://www.twitter.com/',
  vimeoUrl:'https://vimeo.com/',
  bingUrl :'https://www.bing.com/',
  uploadsSponser : '/uploads/sponser/',
  uploadsLogo : '/uploads/logo/',
  uploadsBuilding : '/uploads/building_files/',
  uploadsInspector : '/uploads/inspector/',
  uploadsQuestion : '/uploads/questions/'
};


/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
