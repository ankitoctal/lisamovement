export const environment = {
  production: true,
  siteName :'Lisamovement',
  apiUrl : 'http://14.98.110.245:4006/adminapi', 
  serverUrl : 'http://14.98.110.245:4006',
  instagramUrl : 'https://www.instagram.com/',
  facebookUrl : 'https://www.facebook.com/',
  linkedinUrl : 'https://www.linkedin.com/',
  twitterUrl : 'https://www.twitter.com/',
  vimeoUrl:'https://vimeo.com/',
  bingUrl :'https://www.bing.com/',
  uploadsLogo : '/uploads/logo/',
  uploadsBuilding : '/uploads/building_files/',
  uploadsInspector : '/uploads/inspector/',
  uploadsQuestion : '/uploads/questions/'
};
